﻿using Microsoft.WindowsAzure.StorageClient.Protocol;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkPull
{
    public class Azuretableoperation
    {
        public Task<string> PmsCompanyDataLog(string AzureAccountName, string AzureStorageKey, string AzureTable, PMSDataCompanyRequest pMSDataCompanyRequest)// List<PMSDataCompanyRequest> pMSDataCompanyRequest)
        {


            var UploadedbolbURL = string.Empty;


            var incommingModelRequest = JsonConvert.SerializeObject(pMSDataCompanyRequest.XmlMessages);

            UploadedbolbURL = BlobStorage.UploadBlobWithRestAPI(AzureAccountName, AzureStorageKey,
               Guid.NewGuid().ToString(), "", pMSDataCompanyRequest.PropertyCode.ToString(), "fxdist-bulkdata", incommingModelRequest).Result;

            pMSDataCompanyRequest.XmlMessages = UploadedbolbURL;

            var _result = string.Empty;
            var context = new DynamicTableContext(AzureTable, new Credentials(AzureAccountName, AzureStorageKey));
            _result = context.Insert(pMSDataCompanyRequest);
            return Task.FromResult(_result);
        }


        public  Task<string> UpdatePmsCompanyDataLog(string AzureAccountName, string AzureStorageKey, string AzureTable, PMSDataCompanyRequest pMSDataCompanyRequest)
        {
            var incommingModelRequest = JsonConvert.SerializeObject(pMSDataCompanyRequest.XmlMessages);
            var _result = string.Empty;
            var context = new DynamicTableContext(AzureTable, new Credentials(AzureAccountName, AzureStorageKey));
            _result = context.InsertOrReplace(pMSDataCompanyRequest);
            return  Task.FromResult(_result);
        }


        public Task<string> PmsReservationDataLog(string AzureAccountName, string AzureStorageKey, string AzureTable, PMSDataReservationRequest pMSDataCompanyRequest)// List<PMSDataCompanyRequest> pMSDataCompanyRequest)
        {


            var UploadedbolbURL = string.Empty;


            var incommingModelRequest = JsonConvert.SerializeObject(pMSDataCompanyRequest.XmlMessages);

            UploadedbolbURL = BlobStorage.UploadBlobWithRestAPI(AzureAccountName, AzureStorageKey,
               Guid.NewGuid().ToString(), "", pMSDataCompanyRequest.PropertyCode.ToString(), "fxdist-bulkdata", incommingModelRequest).Result;

            pMSDataCompanyRequest.XmlMessages = UploadedbolbURL;

            var _result = string.Empty;
            var context = new DynamicTableContext(AzureTable, new Credentials(AzureAccountName, AzureStorageKey));
            _result = context.Insert(pMSDataCompanyRequest);
            return Task.FromResult(pMSDataCompanyRequest.RowKey);
        }

        public async Task<string> IsBulkReservationExists(InterfaceConfigModel configurationModel, string PropertyCode)
        {
            configurationModel.AzureTable = "ReservationBulkData";
            string _propertycode = string.Empty;

            var queryString = string.Format("PropertyCode eq " + "'" + PropertyCode + "'");

            var context = new DynamicTableContext(configurationModel.AzureTable, new Credentials(configurationModel.AzureAccountName, configurationModel.AzureStorageKey));

            foreach (KeyValuePair<string, object> keyValuePair in context.ReservationQuery(queryString))
            {

                switch (keyValuePair.Key)
                {
                    case "PropertyCode":
                        _propertycode = Convert.ToString(keyValuePair.Value);
                        break;
                }
            }

            return await Task.FromResult(_propertycode);
        }
    }
}
