namespace BulkPull
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UploadButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_ReservationExport = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.comboDBList = new System.Windows.Forms.ComboBox();
            this.lblDb = new System.Windows.Forms.Label();
            this.btnClearLog = new System.Windows.Forms.Button();
            this.btnSaveInFile = new System.Windows.Forms.Button();
            this.rtStatus = new System.Windows.Forms.RichTextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // UploadButton
            // 
            this.UploadButton.Location = new System.Drawing.Point(230, 21);
            this.UploadButton.Name = "UploadButton";
            this.UploadButton.Size = new System.Drawing.Size(233, 33);
            this.UploadButton.TabIndex = 13;
            this.UploadButton.Text = "Export";
            this.UploadButton.UseVisualStyleBackColor = true;
            this.UploadButton.Click += new System.EventHandler(this.UploadButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.CausesValidation = false;
            this.groupBox1.Controls.Add(this.UploadButton);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 72);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(469, 80);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Company Profile";
            // 
            // groupBox2
            // 
            this.groupBox2.CausesValidation = false;
            this.groupBox2.Controls.Add(this.btn_ReservationExport);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 158);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(469, 80);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Reservation";
            // 
            // btn_ReservationExport
            // 
            this.btn_ReservationExport.Location = new System.Drawing.Point(230, 31);
            this.btn_ReservationExport.Name = "btn_ReservationExport";
            this.btn_ReservationExport.Size = new System.Drawing.Size(233, 33);
            this.btn_ReservationExport.TabIndex = 13;
            this.btn_ReservationExport.Text = "Export";
            this.btn_ReservationExport.UseVisualStyleBackColor = true;
            this.btn_ReservationExport.Click += new System.EventHandler(this.btn_ReservationExport_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.CausesValidation = false;
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(12, 244);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(469, 80);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Guest Profile";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(230, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(233, 33);
            this.button2.TabIndex = 13;
            this.button2.Text = "Export";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // comboDBList
            // 
            this.comboDBList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboDBList.FormattingEnabled = true;
            this.comboDBList.Location = new System.Drawing.Point(142, 25);
            this.comboDBList.Name = "comboDBList";
            this.comboDBList.Size = new System.Drawing.Size(339, 23);
            this.comboDBList.TabIndex = 17;
            this.comboDBList.SelectedIndexChanged += new System.EventHandler(this.comboDBList_SelectedIndexChanged);
            // 
            // lblDb
            // 
            this.lblDb.AutoSize = true;
            this.lblDb.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDb.Location = new System.Drawing.Point(23, 26);
            this.lblDb.Name = "lblDb";
            this.lblDb.Size = new System.Drawing.Size(104, 16);
            this.lblDb.TabIndex = 16;
            this.lblDb.Text = "Bulk Data Type:";
            // 
            // btnClearLog
            // 
            this.btnClearLog.Location = new System.Drawing.Point(627, 549);
            this.btnClearLog.Name = "btnClearLog";
            this.btnClearLog.Size = new System.Drawing.Size(75, 23);
            this.btnClearLog.TabIndex = 19;
            this.btnClearLog.Text = "Clear Log";
            this.btnClearLog.UseVisualStyleBackColor = true;
            this.btnClearLog.Click += new System.EventHandler(this.btnClearLog_Click);
            // 
            // btnSaveInFile
            // 
            this.btnSaveInFile.Location = new System.Drawing.Point(546, 549);
            this.btnSaveInFile.Name = "btnSaveInFile";
            this.btnSaveInFile.Size = new System.Drawing.Size(75, 23);
            this.btnSaveInFile.TabIndex = 20;
            this.btnSaveInFile.Text = "Save Log";
            this.btnSaveInFile.UseVisualStyleBackColor = true;
            // 
            // rtStatus
            // 
            this.rtStatus.Location = new System.Drawing.Point(12, 345);
            this.rtStatus.Name = "rtStatus";
            this.rtStatus.Size = new System.Drawing.Size(483, 85);
            this.rtStatus.TabIndex = 21;
            this.rtStatus.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 442);
            this.Controls.Add(this.rtStatus);
            this.Controls.Add(this.btnSaveInFile);
            this.Controls.Add(this.btnClearLog);
            this.Controls.Add(this.comboDBList);
            this.Controls.Add(this.lblDb);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bulk Data Process";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button UploadButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_ReservationExport;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox comboDBList;
        private System.Windows.Forms.Label lblDb;
        private System.Windows.Forms.Button btnClearLog;
        private System.Windows.Forms.Button btnSaveInFile;
        private System.Windows.Forms.RichTextBox rtStatus;
    }
}

