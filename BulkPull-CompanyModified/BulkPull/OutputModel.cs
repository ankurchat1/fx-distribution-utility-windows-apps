﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace BulkPull
{
    /// <summary>
    /// Output request model 
    /// </summary>
    public class OutputResponseModel
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public object Response { get; set; }
    }
    public class InventoryModelResponse
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public object Response { get; set; }
    }
   


    /// <summary>
    /// Output Handler 
    /// </summary>
    public class OutputHandler
    {
        public static void OutputSuccessResponse<T>(object model, bool IsList, ref OutputResponseModel outputModel)
        {
            var _CheckObjectType = model.GetType();
            if (!IsList)
            {
                if (model is DataTable)
                {
                    var _SqlResponse = model as DataTable;
                    if (_SqlResponse.Rows.Count > 0)
                    {
                        if (_SqlResponse.Rows[0][0].ToString() == "0")
                        {
                            outputModel.Status = 1;
                            outputModel.Response = _SqlResponse.Rows[0][1].ToString();
                            outputModel.Message = MessageModel.SUCCESS;
                        }
                        else
                        {
                            outputModel.Status = 0;
                            outputModel.Response = _SqlResponse.Rows[0][1].ToString();
                            outputModel.Message = MessageModel.ADMINISTRTOR_ERROR;
                        }
                    }
                }
                else if (model is string)
                {
                    outputModel.Status = 1;
                    outputModel.Response = model;
                    outputModel.Message = MessageModel.SUCCESS;
                }
            }
            else
            {
                outputModel.Status = 2;
                outputModel.Message = MessageModel.NO_RECORD_FOUND;
                outputModel.Response = outputModel.Message;
                var _SqlResponse = model as List<T>;
                if (_SqlResponse != null)
                {
                    if (_SqlResponse.Count > 0)
                    {
                        outputModel.Status = 1;
                        outputModel.Message = string.Format("Total {0} Record Found(s)", _SqlResponse.Count);
                        outputModel.Response = _SqlResponse;
                    }
                }
            }
        }

        public static void OutputErrorResponse(Exception ex, ref OutputResponseModel outputModel)
        {
            outputModel.Status = 0;
            outputModel.Response = ex.Message.ToString();
            outputModel.Message = MessageModel.ADMINISTRTOR_ERROR;
        }

    }
}
