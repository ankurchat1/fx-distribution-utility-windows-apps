using BulkPull.Model;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

// This is the code for your desktop app.
// Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.

namespace BulkPull
{
    public partial class Form1 : Form
    {
        private string _dbConnection = string.Empty; // ConfigurationManager.AppSettings["DBConnection"]; 
        //private string _dbConnection = ConfigurationManager.AppSettings["DBConnection"]; 
        public Form1()
        {
            InitializeComponent();
        }

        public Form1(string _connecttion)
        {
            _dbConnection = _connecttion;
            InitializeComponent();
        }



        public static string _filePath { get; set; }
        public static string _targetSQLDb { get; set; }

        private void UploadButton_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            string startTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");


            UploadButton.Enabled = false;
            UploadButton.ForeColor = Color.Green;


            string _Output = string.Empty;
            var _OutputResponse = new OutputResponseModel();
            DataSet DataSet = new DataSet();
            Dictionary<string, object> ResponseData = new Dictionary<string, object>();
            try
            {
                var AzureAccountName = ConfigurationManager.AppSettings["AzureAccountName"];
                var AzureStorageKey = ConfigurationManager.AppSettings["AzureStorageKey"];
                var AzureStorageTable = ConfigurationManager.AppSettings["AzureStorageCompanyTable"];
                var AzureCompanyQueue = ConfigurationManager.AppSettings["AzureCompanyQueue"];
                var AzureServiceBus = ConfigurationManager.AppSettings["ServiceBus"];

                string _outResponse = string.Empty;

                // getting data from database 
                string connetionString = null;
                SqlConnection connection;
                SqlDataAdapter adapter;
                SqlCommand command = new SqlCommand();
                DataSet ds = new DataSet();
                DataSet dsComCodeRecord = new DataSet();
                SqlDataAdapter adapterComcodeRecord;

                connetionString = _dbConnection;
                connection = new SqlConnection(connetionString);
                connection.Open();
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "[DBO].[CompanyBulkComCodeCount]";
                adapterComcodeRecord = new SqlDataAdapter(command);
                adapterComcodeRecord.Fill(dsComCodeRecord);

                List<BulkRequestCompanyComCodeCount> lstComCount = dsComCodeRecord.Tables[0].AsEnumerable().Select(
                            dataRow => new BulkRequestCompanyComCodeCount
                            {
                                rownub = dataRow.Field<int>("rownub"),                                
                                comcod = dataRow.Field<string>("comcod"),
                                comtyp = dataRow.Field<string>("comtyp"),                               
                            }).ToList();

                connection.Close();

                int _recordCount = 0;
                int _totalRecords = _recordCount + 10;                

                var _filteredRecords = lstComCount.Where(x => x.rownub > _recordCount+1 && x.rownub<_totalRecords).ToList();

                var xmlfromLINQ = new XElement("CompanyBulk",
            from c in _filteredRecords
            select new XElement("CompanyCount",
                new XElement("rownub", c.rownub),
                new XElement("comcod", c.comcod),
                new XElement("comtyp", c.comtyp)                
                ));

                string str = xmlfromLINQ.ToString();
                
                ExecuteCompanyBatchData();

                //XmlDocument companyxml = new XmlDocument();
                //companyxml.LoadXml(str);



                //connetionString = _dbConnection;
                //connection = new SqlConnection(connetionString);
                //connection.Open();
                //command.Connection = connection;
                //command.CommandType = CommandType.StoredProcedure;
                ////command.Parameters.Add((Convert.ToString(str),SqlDbType.Xml,40000));             
                //command.Parameters.AddWithValue("@companyxml", str);


                //command.CommandText = "[PMS].[SP_CompnayBulkPull_XML]";
                //adapter = new SqlDataAdapter(command);
                //adapter.Fill(ds);

                //// data has fetched from db

                //List<PMSDataCompanyRequest> lstPmsCompany = ds.Tables[0].AsEnumerable().Select(
                //            dataRow => new PMSDataCompanyRequest
                //            {
                //                SerialNo = dataRow.Field<int>("SRLNUB"),
                //                PropertyCode = dataRow.Field<long>("PMSCODE"),
                //                ComCode = dataRow.Field<string>("COMCOD"),
                //                XmlMessages = dataRow.Field<string>("XMLMESSAGE"),
                //                ProcessTime = DateTime.Now.ToString()
                //            }).ToList();

                //connection.Close();

                //Azuretableoperation azuretableoperation = new Azuretableoperation();
                //ServiceBusQueueMessage serviceBusQueueMessage = new ServiceBusQueueMessage();

                //foreach (var item in lstPmsCompany)
                //{
                //    string xmlmessage = string.Empty;
                //    xmlmessage = item.XmlMessages;
                //    item.PartitionKey = Convert.ToString(item.PropertyCode);

                //    _outResponse = azuretableoperation.PmsCompanyDataLog(AzureAccountName, AzureStorageKey, AzureStorageTable, item).Result;

                //    //serviceBusQueueMessage.XMLMessageUrl = item.XmlMessages;


                //    //string QueueMessage = JsonConvert.SerializeObject(serviceBusQueueMessage);

                //    // BulkRequestCompany incomming = new BulkRequestCompany();                   
                //    // incomming.Data = message;
                //    // incomming.PmsCustCode = Convert.ToString(item.PropertyCode);
                //    // incomming.Token = item.RowKey;

                //    //// string _CompanyBulkAPI = "https://crsbulkcompany.azurewebsites.net/api/BulkCompany/DataProcess";
                //    // string _CompanyBulkAPI = "https://crsbulkcompanyprod.azurewebsites.net/api/BulkCompany/DataProcess";

                //    // //
                //    // //
                //    // string _CompanyBulkAPI = "http://localhost:50021/api/BulkCompany/DataProcess";


                //    //try
                //    //{
                //    //    var _objTojson = JsonConvert.SerializeObject(incomming);
                //    //    var result = SharkPost(_CompanyBulkAPI, incomming);

                //    //    Thread.Sleep(10);
                //    //    //item.MessageStatus = 
                //    //    //_outResponse = azuretableoperation.UpdatePmsCompanyDataLog(AzureAccountName, AzureStorageKey, AzureStorageTable, item).Result;


                //    //}
                //    //catch (Exception ex)
                //    //{
                //    //    OutputHandler.OutputErrorResponse(ex, ref _OutputResponse);
                //    //}


                //    try
                //    {
                //        var _output = SendToCompanyGateway(xmlmessage, item.PropertyCode.ToString(), _outResponse);

                //        if (_output.MESSAGE == "Success")
                //        {

                //        }

                //        //rtStatus.Text = string.Format( "Reservation {0}",DateTime.Now.ToString("yyyy-MM-dd HHmm ss"));
                //        //rtStatus.ForeColor = Color.Green;
                //        //rtStatus.BackColor = Color.White;

                //        rtStatus.Text = sb.Append(startTime).ToString();
                //        rtStatus.Text = sb.Append("Company Trasfering... \n").ToString();
                //        rtStatus.ForeColor = Color.Green;
                //        rtStatus.BackColor = Color.White;

                //        Thread.Sleep(5000);

                //    }
                //    catch (Exception ex)
                //    {
                //        OutputHandler.OutputErrorResponse(ex, ref _OutputResponse);
                //    }

                //}

                MessageBox.Show(" Data Exported Successfully !! ");
            }
            catch (Exception ex)
            {
                _Output = ex.Message.ToString();
            }
        }

        private string ExecuteCompanyBatchData()
        {
            connetionString = _dbConnection;
            connection = new SqlConnection(connetionString);
            connection.Open();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            //command.Parameters.Add((Convert.ToString(str),SqlDbType.Xml,40000));             
            command.Parameters.AddWithValue("@companyxml", str);


            command.CommandText = "[PMS].[SP_CompnayBulkPull_XML]";
            adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);

            // data has fetched from db

            List<PMSDataCompanyRequest> lstPmsCompany = ds.Tables[0].AsEnumerable().Select(
                        dataRow => new PMSDataCompanyRequest
                        {
                            SerialNo = dataRow.Field<int>("SRLNUB"),
                            PropertyCode = dataRow.Field<long>("PMSCODE"),
                            ComCode = dataRow.Field<string>("COMCOD"),
                            XmlMessages = dataRow.Field<string>("XMLMESSAGE"),
                            ProcessTime = DateTime.Now.ToString()
                        }).ToList();

            connection.Close();

            Azuretableoperation azuretableoperation = new Azuretableoperation();
            ServiceBusQueueMessage serviceBusQueueMessage = new ServiceBusQueueMessage();

            foreach (var item in lstPmsCompany)
            {
                string xmlmessage = string.Empty;
                xmlmessage = item.XmlMessages;
                item.PartitionKey = Convert.ToString(item.PropertyCode);

                _outResponse = azuretableoperation.PmsCompanyDataLog(AzureAccountName, AzureStorageKey, AzureStorageTable, item).Result;

                //serviceBusQueueMessage.XMLMessageUrl = item.XmlMessages;


                //string QueueMessage = JsonConvert.SerializeObject(serviceBusQueueMessage);

                // BulkRequestCompany incomming = new BulkRequestCompany();                   
                // incomming.Data = message;
                // incomming.PmsCustCode = Convert.ToString(item.PropertyCode);
                // incomming.Token = item.RowKey;

                //// string _CompanyBulkAPI = "https://crsbulkcompany.azurewebsites.net/api/BulkCompany/DataProcess";
                // string _CompanyBulkAPI = "https://crsbulkcompanyprod.azurewebsites.net/api/BulkCompany/DataProcess";

                // //
                // //
                // string _CompanyBulkAPI = "http://localhost:50021/api/BulkCompany/DataProcess";


                //try
                //{
                //    var _objTojson = JsonConvert.SerializeObject(incomming);
                //    var result = SharkPost(_CompanyBulkAPI, incomming);

                //    Thread.Sleep(10);
                //    //item.MessageStatus = 
                //    //_outResponse = azuretableoperation.UpdatePmsCompanyDataLog(AzureAccountName, AzureStorageKey, AzureStorageTable, item).Result;


                //}
                //catch (Exception ex)
                //{
                //    OutputHandler.OutputErrorResponse(ex, ref _OutputResponse);
                //}


                try
                {
                    var _output = SendToCompanyGateway(xmlmessage, item.PropertyCode.ToString(), _outResponse);

                    if (_output.MESSAGE == "Success")
                    {

                    }

                    //rtStatus.Text = string.Format( "Reservation {0}",DateTime.Now.ToString("yyyy-MM-dd HHmm ss"));
                    //rtStatus.ForeColor = Color.Green;
                    //rtStatus.BackColor = Color.White;

                    rtStatus.Text = sb.Append(startTime).ToString();
                    rtStatus.Text = sb.Append("Company Trasfering... \n").ToString();
                    rtStatus.ForeColor = Color.Green;
                    rtStatus.BackColor = Color.White;

                    Thread.Sleep(5000);

                }
                catch (Exception ex)
                {
                    OutputHandler.OutputErrorResponse(ex, ref _OutputResponse);
                }

            }
        }



        private async Task QueueExists(string QueueName, string ServiceBusConnectionString)
        {
            using (var _QueueHandler = new QueueHandler(ServiceBusConnectionString))
            {
                await _QueueHandler.QueueExists(QueueName);
            }
        }

        private void GetFuncationalityValue()
        {

            comboDBList.Items.Add("--Select--");
            comboDBList.Items.Add("Company");
            comboDBList.Items.Add("Reservation");
            comboDBList.Items.Add("Guest");
            comboDBList.SelectedIndex = 0;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            string DB = _dbConnection;
            GetFuncationalityValue();
        }

        private void comboDBList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboDBList.SelectedIndex == 0)
            {
                groupBox1.Enabled = true;
                groupBox2.Enabled = true;
                groupBox3.Enabled = true;
            }
            else if (comboDBList.SelectedIndex == 1)
            {
                groupBox1.Enabled = true;
                groupBox2.Enabled = false;
                groupBox3.Enabled = false;


            }
            else if (comboDBList.SelectedIndex == 2)
            {
                groupBox1.Enabled = false;
                groupBox2.Enabled = true;
                groupBox3.Enabled = false;
            }
            else
            {
                groupBox1.Enabled = false;
                groupBox2.Enabled = false;
                groupBox3.Enabled = true;
            }


            using (StreamWriter w = File.AppendText("log.txt"))
            {
                LogControl.Log(comboDBList.SelectedText.ToString(), w);
            }

            using (StreamReader r = File.OpenText("log.txt"))
            {
                StringBuilder sb = new StringBuilder();
                string line;
                while ((line = r.ReadLine()) != null)
                {
                    sb.AppendLine(line.ToString());
                }

                //rtLog.Text = sb.ToString();
            }




        }

        private void btn_ReservationExport_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            string startTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");


            btn_ReservationExport.Enabled = false;
            btn_ReservationExport.ForeColor = Color.Green;


            string _Output = string.Empty;
            var _OutputResponse = new OutputResponseModel();
            DataSet DataSet = new DataSet();
            Dictionary<string, object> ResponseData = new Dictionary<string, object>();
            try
            {
                var AzureAccountName = ConfigurationManager.AppSettings["AzureAccountName"];
                var AzureStorageKey = ConfigurationManager.AppSettings["AzureStorageKey"];
                var AzureStorageTable = ConfigurationManager.AppSettings["AzureStorageReservationTable"];
                var AzureCompanyQueue = ConfigurationManager.AppSettings["AzureReservationQueue"];
                var AzureServiceBus = ConfigurationManager.AppSettings["ServiceBus"];

                string _outResponse = string.Empty;

                // getting data from database 

                string connetionString = null;
                SqlConnection connection;
                SqlDataAdapter adapter;
                SqlCommand command = new SqlCommand();
                DataSet ds = new DataSet();

                connetionString = _dbConnection;
                StringBuilder sqlQuery = new StringBuilder();
                connection = new SqlConnection(connetionString);
                try
                {
                    sqlQuery = DropprocedureString();

                    connection.Open();
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = sqlQuery.ToString();
                    adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                    connection.Close();
                    MessageBox.Show("procedure has dropped");
                }
                catch (Exception ex)
                {

                    MessageBox.Show("Dropped" + ex.Message);
                    connection.Close();
                }




                sqlQuery.Clear();

                try
                {
                    sqlQuery = procedureString();
                    connection.Open();
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = sqlQuery.ToString();
                    adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                    connection.Close();
                    MessageBox.Show("Created the procedure");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Created" + ex.Message);
                    connection.Close();
                }


                try
                {
                    connection.Open();
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "[PMS].[SP_ReservationBulkPull_XML]";
                    adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                    connection.Close();
                    MessageBox.Show("Executed the procedure");
                }
                catch (Exception ex)
                {
                    connection.Close();
                    MessageBox.Show("Executed" + ex.Message);
                }


                // data has fetched from db

                List<PMSDataReservationRequest> lstPmsReservation = ds.Tables[0].AsEnumerable().Select(
                            dataRow => new PMSDataReservationRequest
                            {
                                SerialNo = dataRow.Field<int>("ROWNUB"),
                                PropertyCode = dataRow.Field<long>("PMSCODE"),
                                ReservationNumber = dataRow.Field<decimal>("RESNUB"),
                                XmlMessages = dataRow.Field<string>("XMLMESSAGE"),
                                ProcessTime = DateTime.Now.ToString()
                            }).ToList();



                Azuretableoperation azuretableoperation = new Azuretableoperation();
                ServiceBusQueueMessage serviceBusQueueMessage = new ServiceBusQueueMessage();



                //lblReservationStatus.Text = "Total " + lstPmsReservation.Count().ToString() + " Number Received By PMS";
                rtStatus.Text = sb.Append(startTime).ToString();
                rtStatus.Text = sb.Append("   Reservation Process Initialize.... \n").ToString();
                rtStatus.ForeColor = Color.Green;
                rtStatus.BackColor = Color.White;

                Thread.Sleep(3000);

                //rtStatus.Text = "Reservation Transfer Process started PMS to FXCRS";
                rtStatus.Text = sb.Append(startTime).ToString();
                rtStatus.Text = sb.Append("Reservation Transfer Process started PMS to FXCRS \n").ToString();
                rtStatus.ForeColor = Color.Green;
                rtStatus.BackColor = Color.White;

                /// Azure configuration table
                InterfaceConfigModel interfacemodel = new InterfaceConfigModel();
                interfacemodel.AzureTable = AzureStorageTable.ToString();
                interfacemodel.AzureAccountName = AzureAccountName.ToString();
                interfacemodel.AzureStorageKey = AzureStorageKey.ToString();
                string _propertycodeexist = string.Empty;


                foreach (var item in lstPmsReservation)
                {
                    string xmlmessage = string.Empty;
                    xmlmessage = item.XmlMessages;

                    var records = azuretableoperation.IsBulkReservationExists(interfacemodel, item.PropertyCode.ToString());
                    _propertycodeexist = records.Result;

                    //if (Convert.ToString(item.PropertyCode) == _propertycodeexist)
                    //{
                    //    MessageBox.Show("Bulk Pull Already executed for " + _propertycodeexist + "Property");
                    //}




                    _outResponse = azuretableoperation.PmsReservationDataLog(AzureAccountName, AzureStorageKey,
                        AzureStorageTable, item).Result;

                    try
                    {
                        var _output = SendToGateway(xmlmessage, item.PropertyCode.ToString(), _outResponse);

                        if (_output.MESSAGE == "Success")
                        {

                        }

                        //rtStatus.Text = string.Format( "Reservation {0}",DateTime.Now.ToString("yyyy-MM-dd HHmm ss"));
                        //rtStatus.ForeColor = Color.Green;
                        //rtStatus.BackColor = Color.White;

                        rtStatus.Text = sb.Append(startTime).ToString();
                        rtStatus.Text = sb.Append("Reservation Trasfering... \n").ToString();
                        rtStatus.ForeColor = Color.Green;
                        rtStatus.BackColor = Color.White;

                        Thread.Sleep(5000);

                    }
                    catch (Exception ex)
                    {
                        OutputHandler.OutputErrorResponse(ex, ref _OutputResponse);
                    }

                }
                btn_ReservationExport.Enabled = true;
                btn_ReservationExport.ForeColor = Color.Black;
                MessageBox.Show(" Data Exported Successfully !! ");
            }
            catch (Exception ex)
            {
                _Output = ex.Message.ToString();
            }
        }

        private StringBuilder DropprocedureString()
        {
            StringBuilder sb = new StringBuilder();


            sb.AppendLine(" IF EXISTS( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PMS].[SP_ReservationBulkPull_XML]') AND type IN ( N'P', N'PC') ) ");
            sb.AppendLine("  begin  ");
            sb.AppendLine(" DROP procedure [PMS].[SP_ReservationBulkPull_XML] ;");
            sb.AppendLine(" END");


            return sb;
        }

        private StringBuilder procedureString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("  create PROCEDURE [PMS].[SP_ReservationBulkPull_XML] AS BEGIN SET NOCOUNT ON DECLARE @PRPCOD VARCHAR(3) DECLARE @PMSCUSTCODE BIGINT DECLARE @ECOTOKEN NVARCHAR(600) DECLARE @DATETIME NVARCHAR(100)=(SELECT CONVERT(VARCHAR(23), GETDATE(), 126)) SELECT TOP(1) @PRPCOD=PRPCOD FROM pms.PR002TBL WHERE DELFLG=0 select @PMSCUSTCODE=CSTCOD from pms.GNMANTBL DECLARE @BULKRESERVATION AS TABLE(ROWNUB INT,PMSCODE BIGINT,RESNUB DECIMAL,SRLNUB DECIMAL,SUBSRL DECIMAL,XMLMESSAGE XML,CREATEDTIME DATETIME) DECLARE @RESERVATIONTABLE AS TABLE(ROWNUB INT, RESNUB DECIMAL,SRLNUB DECIMAL,SUBSRL DECIMAL) DECLARE @RESERVATIONTBL AS TABLE(RESNUB DECIMAL,ROWNUB INT) INSERT INTO @RESERVATIONTABLE SELECT DISTINCT ROW_NUMBER() OVER(ORDER BY RESNUB ASC),RESNUB,SRLNUB,SUBSRL FROM FMR01TBL A WHERE A.SUBSRL = (SELECT MAX(B.SUBSRL) FROM FMR01TBL B WHERE A.PRPCOD=B.PRPCOD AND A.RESNUB = B.RESNUB AND A.SRLNUB = B.SRLNUB) AND (A.SGLBKD +A.DBLBKD+ A.TPLBKD+A.QUDBKD) <> 0 AND A.ARRDAT >= (SELECT ACCDAT FROM pms.GNACDTBL) AND A.CKIROM = 0 AND A.CKIPAX = 0 INSERT INTO @RESERVATIONTBL SELECT RESNUB, ROW_NUMBER() OVER(ORDER BY RESNUB ASC) AS ROWNUB FROM(SELECT DISTINCT RESNUB FROM @RESERVATIONTABLE) A DECLARE @TOTALROW INT=0 DECLARE @CURRENTROW INT=1 SELECT @TOTALROW=COUNT(1) FROM @RESERVATIONTBL select * into #tmpFmr01tbl from pms.fmr01tbl where 1=2 select * into #tmpFmr14tbl from pms.fmr14tbl where 1=2 WHILE (@CURRENTROW<=@TOTALROW) BEGIN DECLARE @CUR_RESNUB decimal=0 DECLARE @CUR_SRLNUB decimal=0 DECLARE @CUR_SUBSRL decimal=0 delete from #tmpFmr01tbl delete from #tmpFmr14tbl DECLARE @MAINXML XML='' DECLARE @PROPERTY XML ='' DECLARE @FMR00TBL XML='' DECLARE @FMR01TBL XML='' DECLARE @FMR02TBL XML='' DECLARE @FMR03TBL XML='' DECLARE @FMR04TBL XML='' DECLARE @FMR05TBL XML='' DECLARE @FMR06TBL XML='' DECLARE @FMR07TBL XML='' DECLARE @FMR08TBL XML='' DECLARE @FMR09TBL XML='' DECLARE @FMR10TBL XML='' DECLARE @FMR11TBL XML='' DECLARE @FMR12TBL XML='' DECLARE @FMR13TBL XML='' DECLARE @FMR14TBL XML='' DECLARE @FMR15TBL XML='' DECLARE @FMR16TBL XML='' DECLARE @FMR17TBL XML='' DECLARE @FMR18TBL XML='' DECLARE @FMR19TBL XML='' DECLARE @FMR25TBL XML='' DECLARE @FMR26TBL XML='' DECLARE @FMRDCTBL XML='' DECLARE @FMRSNTBL XML='' DECLARE @FMRCHTBL XML='' DECLARE @CM_BOOKING_COMMENTS XML='' SELECT @ECOTOKEN=NEWID() SET @MAINXML='<SKYRESDATA><PROPERTYDETAILS></PROPERTYDETAILS></SKYRESDATA>' SET @PROPERTY='<APPLICATIONNAME>SKYRES</APPLICATIONNAME> <PRPCOD>'+@PRPCOD+'</PRPCOD> <PMSCODE>'+CAST(@PRPCOD AS NVARCHAR(MAX))+'</PMSCODE> <PLATFORMCODE>'+CAST(@PRPCOD AS NVARCHAR(MAX))+'</PLATFORMCODE> <CHAINCODE>'+CAST(@PRPCOD AS NVARCHAR(MAX))+'</CHAINCODE> <TABLENAME>RESERVATION</TABLENAME> <OPERATIONMODE>INSERT</OPERATIONMODE> <ECOTOKENNO>'+@ECOTOKEN+'</ECOTOKENNO> <TIMESTAMP>'+@DATETIME+'</TIMESTAMP>' SELECT @CUR_RESNUB= RESNUB FROM @RESERVATIONTBL WHERE ROWNUB=@CURRENTROW insert into #tmpFmr01tbl SELECT * FROM FMR01TBL A WHERE A.SUBSRL = (SELECT MAX(B.SUBSRL) FROM FMR01TBL B WHERE A.PRPCOD=B.PRPCOD AND A.RESNUB = B.RESNUB AND A.SRLNUB = B.SRLNUB) AND RESNUB=@CUR_RESNUB update #tmpFmr01tbl set subsrl=1 insert into #tmpFmr14tbl SELECT * FROM FMR14TBL A WHERE A.SUBSRL = (SELECT MAX(B.SUBSRL) FROM FMR01TBL B WHERE A.RESNUB = B.RESNUB AND A.SRLNUB = B.SRLNUB) AND RESNUB=@CUR_RESNUB update #tmpFmr14tbl set subsrl=1 set @FMR00TBL=(SELECT * FROM FMR00TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR01TBL=(SELECT * FROM #tmpFmr01tbl as FMR01TBL FOR XML AUTO,ELEMENTS) SET @FMR02TBL=(SELECT * FROM FMR02TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR03TBL=(SELECT * FROM FMR03TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR04TBL=(SELECT * FROM FMR04TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR05TBL=(SELECT * FROM FMR05TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR06TBL=(SELECT * FROM FMR06TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR07TBL=(SELECT * FROM FMR07TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR08TBL=(SELECT * FROM FMR08TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR09TBL=(SELECT * FROM FMR09TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR10TBL=(SELECT * FROM FMR10TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR11TBL=(SELECT * FROM FMR11TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR12TBL=(SELECT * FROM FMR12TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR13TBL=(SELECT * FROM FMR13TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR14TBL=(SELECT * FROM #tmpFmr14tbl FMR14TBL FOR XML AUTO,ELEMENTS) SET @FMR15TBL=(SELECT * FROM FMR15TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR16TBL=(SELECT * FROM FMR16TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR17TBL=(SELECT * FROM FMR17TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR18TBL=(SELECT * FROM FMR18TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR19TBL=(SELECT * FROM FMR19TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR25TBL=(SELECT * FROM FMR25TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR26TBL=(SELECT * FROM FMR26TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMRDCTBL=(SELECT * FROM FMRDCTBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMRSNTBL=(SELECT * FROM FMRSNTBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMRCHTBL=(SELECT * FROM FMRCHTBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @MAINXML.modify('insert sql:variable(\"@PROPERTY\") as first into (/SKYRESDATA/PROPERTYDETAILS)[1]') SET @MAINXML.modify('insert sql:variable(\"@FMR00TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR01TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR02TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR03TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR04TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR05TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR06TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR07TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR08TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR09TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR10TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR11TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR12TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR13TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR14TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR15TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR16TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR17TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR18TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR19TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR25TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR26TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMRDCTBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMRSNTBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMRCHTBL\") as last into (/SKYRESDATA[1])') INSERT INTO @BULKRESERVATION SELECT @CURRENTROW,@PMSCUSTCODE,@CUR_RESNUB,@CUR_SRLNUB,@CUR_SUBSRL,@MAINXML,GETDATE() SET @CURRENTROW+=1 END SELECT * FROM @BULKRESERVATION ORDER BY 1 ASC END ");

            return sb;
        }



        private DHResponseModel SendToGateway(string IncommingXMlMessage, string pmscode, string tokenNumber)
        {

            string url = string.Empty;
            string content = string.Empty;

            // content = new WebClient().DownloadString(IncommingXMlMessage);

            rtStatus.Text = "Reservation Transfer Process Processing....";
            rtStatus.ForeColor = Color.Green;
            rtStatus.BackColor = Color.White;

            DHResponseModel dHResponseModel = new DHResponseModel();
            //IncommingModel incomming = new IncommingModel();
            //incomming.PMSCUSTCODE = Convert.ToInt64(pmscode);
            //incomming.PMSCUSTID = 0;
            //incomming.TOKENNUMBER = Guid.NewGuid().ToString();
            //incomming.INCOMINGMESSAGE = IncommingXMlMessage; //JsonConvert.SerializeObject(IncommingXMlMessage);
            //incomming.RECEIVEDDATE = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
            //incomming.DATASOURCE = "DATAHUB";
            //incomming.DATADESTINATION = "FXFD";
            //incomming.MESSAGETYPE = "Reservation";

            IncomingMessageModel incomming = new IncomingMessageModel();
            incomming.DhXmlMessage = IncommingXMlMessage;
            incomming.PMSCustCode = Convert.ToInt64(pmscode);
            incomming.TokenNumber = tokenNumber;

            if (content != null)
            {

                //incomming = JsonConvert.DeserializeObject<IncommingModel>(content);

                //string _reservationUrl = "https://fxdistributionreservationgateway.azurewebsites.net/api/InReservation/ReservationData";
                //string _reservationUrl = "https://qafxdistributionreservationgateway.azurewebsites.net/api/InReservation/ReservationData";
                //string _reservationUrl = "https://qacrsbulkreservation.azurewebsites.net/api/BulkReservation/BulkReservation";
                string _reservationUrl = "https://crsbulkreservation.azurewebsites.net/api/BulkReservation/BulkReservation";

                var _objTojson = JsonConvert.SerializeObject(incomming);
                dHResponseModel = SharkPost(_reservationUrl, incomming);

            }
            return dHResponseModel;

        }


        private DHResponseModel SendToCompanyGateway(string IncommingXMlMessage, string pmscode, string tokenNumber)
        {

            string url = string.Empty;
            string content = string.Empty;

            // content = new WebClient().DownloadString(IncommingXMlMessage);

            rtStatus.Text = "Company Transfer Process Processing....";
            rtStatus.ForeColor = Color.Green;
            rtStatus.BackColor = Color.White;

            DHResponseModel dHResponseModel = new DHResponseModel();
            //IncommingModel incomming = new IncommingModel();
            //incomming.PMSCUSTCODE = Convert.ToInt64(pmscode);
            //incomming.PMSCUSTID = 0;
            //incomming.TOKENNUMBER = Guid.NewGuid().ToString();
            //incomming.INCOMINGMESSAGE = IncommingXMlMessage; //JsonConvert.SerializeObject(IncommingXMlMessage);
            //incomming.RECEIVEDDATE = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
            //incomming.DATASOURCE = "DATAHUB";
            //incomming.DATADESTINATION = "FXFD";
            //incomming.MESSAGETYPE = "Reservation";

            IncomingMessageModel incomming = new IncomingMessageModel();
            incomming.DhXmlMessage = IncommingXMlMessage;
            incomming.PMSCustCode = Convert.ToInt64(pmscode);
            incomming.TokenNumber = tokenNumber;

            if (content != null)
            {

                //incomming = JsonConvert.DeserializeObject<IncommingModel>(content);
                string _reservationUrl = "https://crsbulkcompanyprod.azurewebsites.net/api/BulkCompany/DataProcess";

                var _objTojson = JsonConvert.SerializeObject(incomming);
                dHResponseModel = SharkPost(_reservationUrl, incomming);

            }
            return dHResponseModel;

        }


        public DHResponseModel SharkPost(string Url, object model)
        {
            var _Output = new DHResponseModel();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Url);
                    var Records = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                    var response = client.PostAsync(Url, Records).Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        _Output.STATUS = true;
                        _Output.MESSAGE = response.StatusCode.ToString();
                    }
                    else
                    {
                        var responseContent = response.Content;

                        var _pb = responseContent.ReadAsStringAsync();
                        string _OutputConvertToString = _pb.ToString();
                        _Output = JsonConvert.DeserializeObject<DHResponseModel>(_OutputConvertToString);
                    }
                }


            }
            catch (Exception ex)
            {
                _Output.STATUS = false;
                _Output.MESSAGE = ex.Message.ToString();
            }
            return _Output;

        }

        private void btnClearLog_Click(object sender, EventArgs e)
        {
            //rtLog.Text = "";
        }


    }

    public class BulkRequestCompany
    {
        public string Data { get; set; }
        public string PmsCustCode { get; set; }

        public string Token { get; set; }
    }

    public class BulkRequestCompanyComCodeCount 
    {
        public int rownub { get; set; }
        public string comcod { get; set; }
        public string comtyp { get; set; }
    }

    public class PMSDataCompanyRequest
    {
        public string PartitionKey { get; set; } 
        public string RowKey { get; set; } = Guid.NewGuid().ToString();
        public long SerialNo { get; set; }
        public long PropertyCode { get; set; }
        public string ComCode { get; set; }
        public string XmlMessages { get; set; }
        public string ProcessTime { get; set; }
        public string MessageStatus { get; set; }
        public string FXFDMessage { get; set; }
    }
    public class PMSDataReservationRequest
    {
        public string PartitionKey { get; set; } = "9800";
        public string RowKey { get; set; } = Guid.NewGuid().ToString();
        public int SerialNo { get; set; }
        public long PropertyCode { get; set; }
        public decimal ReservationNumber { get; set; }

        public string XmlMessages { get; set; }
        public string ProcessTime { get; set; }
        public string MessageStatus { get; set; }
    }
    public class ServiceBusQueueMessage
    {
        public string PartitionKey { get; set; }
        public string RowKey { get; set; } = Guid.NewGuid().ToString();
        public string XMLMessageUrl { get; set; }

    }
    public class IncomingMessageModel
    {
        public string DhXmlMessage { get; set; }
        public long PMSCustCode { get; set; }
        public string TokenNumber { get; set; }
    }
}
