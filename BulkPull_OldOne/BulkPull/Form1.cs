using BulkPull.Model;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms;

// This is the code for your desktop app.
// Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.

namespace BulkPull
{
    public partial class Form1 : Form
    {
        private string _dbConnection = string.Empty; // ConfigurationManager.AppSettings["DBConnection"]; 
        //private string _dbConnection = ConfigurationManager.AppSettings["DBConnection"]; 
        public Form1()
        {
            InitializeComponent();
        }

        public Form1(string _connecttion)
        {
            _dbConnection = _connecttion;
            InitializeComponent();
        }



        public static string _filePath { get; set; }
        public static string _targetSQLDb { get; set; }

        private void UploadButton_Click(object sender, EventArgs e)
        {
            string _Output = string.Empty;
            var _OutputResponse = new OutputResponseModel();
            DataSet DataSet = new DataSet();
            Dictionary<string, object> ResponseData = new Dictionary<string, object>();
            try
            {
                var AzureAccountName = ConfigurationManager.AppSettings["AzureAccountName"];
                var AzureStorageKey = ConfigurationManager.AppSettings["AzureStorageKey"];
                var AzureStorageTable = ConfigurationManager.AppSettings["AzureStorageCompanyTable"];
                var AzureCompanyQueue = ConfigurationManager.AppSettings["AzureCompanyQueue"];
                var AzureServiceBus = ConfigurationManager.AppSettings["ServiceBus"];

                string _outResponse = string.Empty;

                // getting data from database 

                string connetionString = null;
                SqlConnection connection;
                SqlDataAdapter adapter;
                SqlCommand command = new SqlCommand();
                DataSet ds = new DataSet();

                int i = 0;

                connetionString = _dbConnection;
                connection = new SqlConnection(connetionString);

                connection.Open();
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "[PMS].[SP_CompnayBulkPull_XML]";
                adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);

                // data has fetched from db

                List<PMSDataCompanyRequest> lstPmsCompany = ds.Tables[0].AsEnumerable().Select(
                            dataRow => new PMSDataCompanyRequest
                            {
                                SerialNo = dataRow.Field<int>("SRLNUB"),
                                PropertyCode = dataRow.Field<long>("PMSCODE"),
                                ComCode = dataRow.Field<string>("COMCOD"),
                                XmlMessages = dataRow.Field<string>("XMLMESSAGE"),
                                ProcessTime = DateTime.Now.ToString()
                            }).ToList();

                connection.Close();

                Azuretableoperation azuretableoperation = new Azuretableoperation();
                ServiceBusQueueMessage serviceBusQueueMessage = new ServiceBusQueueMessage();

                foreach (var item in lstPmsCompany)
                {

                    _outResponse = azuretableoperation.PmsCompanyDataLog(AzureAccountName, AzureStorageKey, AzureStorageTable, item).Result;

                    serviceBusQueueMessage.XMLMessageUrl = item.XmlMessages;
                    serviceBusQueueMessage.PartitionKey = item.PropertyCode.ToString();

                    string QueueMessage = JsonConvert.SerializeObject(serviceBusQueueMessage);

                    try
                    {

                        // Check Queue Name Exists or not 
                        QueueExists(AzureCompanyQueue, AzureServiceBus);
                        var _Queue = new QueueClient(AzureServiceBus, AzureCompanyQueue);
                        var _Message = new Microsoft.Azure.ServiceBus.Message(Encoding.UTF8.GetBytes(QueueMessage));

                        ServiceBusOperation.SASKey = "h+sGnq4sEWEd0KPL0gvcwMvvGxtAO74Jx1exFsfMEFA=";
                        ServiceBusOperation.baseAddress = "https://SkyExchange.servicebus.windows.net/";

                        ServiceBusOperation.SendScheduleMessage(AzureCompanyQueue, QueueMessage, "CompanyBulk");
                        Thread.Sleep(10);

                        ////item.MessageStatus = 
                        //_outResponse = azuretableoperation.UpdatePmsCompanyDataLog(AzureAccountName, AzureStorageKey, AzureStorageTable, item).Result;


                    }
                    catch (Exception ex)
                    {
                        OutputHandler.OutputErrorResponse(ex, ref _OutputResponse);
                    }

                }

                MessageBox.Show(" Data Exported Successfully !! ");
            }
            catch (Exception ex)
            {
                _Output = ex.Message.ToString();
            }
        }

        private async Task QueueExists(string QueueName, string ServiceBusConnectionString)
        {
            using (var _QueueHandler = new QueueHandler(ServiceBusConnectionString))
            {
                await _QueueHandler.QueueExists(QueueName);
            }
        }

        private void GetFuncationalityValue()
        {

            comboDBList.Items.Add("--Select--");
            comboDBList.Items.Add("Company");
            comboDBList.Items.Add("Reservation");
            comboDBList.Items.Add("Guest");
            comboDBList.SelectedIndex = 0;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            string DB = _dbConnection;
            GetFuncationalityValue();
        }

        private void comboDBList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboDBList.SelectedIndex == 0)
            {
                groupBox1.Enabled = true;
                groupBox2.Enabled = true;
                groupBox3.Enabled = true;
            }
            else if (comboDBList.SelectedIndex == 1)
            {
                groupBox1.Enabled = true;
                groupBox2.Enabled = false;
                groupBox3.Enabled = false;


            }
            else if (comboDBList.SelectedIndex == 2)
            {
                groupBox1.Enabled = false;
                groupBox2.Enabled = true;
                groupBox3.Enabled = false;
            }
            else
            {
                groupBox1.Enabled = false;
                groupBox2.Enabled = false;
                groupBox3.Enabled = true;
            }


            using (StreamWriter w = File.AppendText("log.txt"))
            {
                LogControl.Log(comboDBList.SelectedText.ToString(), w);
            }

            using (StreamReader r = File.OpenText("log.txt"))
            {
                StringBuilder sb = new StringBuilder();
                string line;
                while ((line = r.ReadLine()) != null)
                {
                    sb.AppendLine(line.ToString());
                }

                rtLog.Text = sb.ToString();
            }




        }

        private void btn_ReservationExport_Click(object sender, EventArgs e)
        {

            btn_ReservationExport.Enabled = false;
            btn_ReservationExport.ForeColor = Color.Green;
            

            string _Output = string.Empty;
            var _OutputResponse = new OutputResponseModel();
            DataSet DataSet = new DataSet();
            Dictionary<string, object> ResponseData = new Dictionary<string, object>();
            try
            {
                var AzureAccountName = ConfigurationManager.AppSettings["AzureAccountName"];
                var AzureStorageKey = ConfigurationManager.AppSettings["AzureStorageKey"];
                var AzureStorageTable = ConfigurationManager.AppSettings["AzureStorageReservationTable"];
                var AzureCompanyQueue = ConfigurationManager.AppSettings["AzureReservationQueue"];
                var AzureServiceBus = ConfigurationManager.AppSettings["ServiceBus"];

                string _outResponse = string.Empty;

                // getting data from database 

                string connetionString = null;
                SqlConnection connection;
                SqlDataAdapter adapter;
                SqlCommand command = new SqlCommand();
                DataSet ds = new DataSet();

                int i = 0;

                connetionString = _dbConnection;
                connection = new SqlConnection(connetionString);

                connection.Open();
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "[PMS].[SP_ReservationBulkPull_XML]";
                adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);

                // data has fetched from db

                List<PMSDataReservationRequest> lstPmsReservation = ds.Tables[0].AsEnumerable().Select(
                            dataRow => new PMSDataReservationRequest
                            {
                                SerialNo = dataRow.Field<int>("ROWNUB"),
                                PropertyCode = dataRow.Field<long>("PMSCODE"),
                                ReservationNumber = dataRow.Field<decimal>("RESNUB"),
                                XmlMessages = dataRow.Field<string>("XMLMESSAGE"),
                                ProcessTime = DateTime.Now.ToString()
                            }).ToList();

                connection.Close();

                Azuretableoperation azuretableoperation = new Azuretableoperation();
                ServiceBusQueueMessage serviceBusQueueMessage = new ServiceBusQueueMessage();

                lblReservationStatus.Text = "Total " + lstPmsReservation.Count().ToString() + " Number Received By PMS";
                lblReservationStatus.ForeColor = Color.Azure;
                lblReservationStatus.BackColor = Color.White;

                Thread.Sleep(3000);

                lblReservationStatus.Text = "Reservation Transfer Process started PMS to FXCRS";
                lblReservationStatus.ForeColor = Color.Green;
                lblReservationStatus.BackColor = Color.White;

                foreach (var item in lstPmsReservation)
                {
                    string xmlmessage = string.Empty;
                    xmlmessage = item.XmlMessages;

                    _outResponse = azuretableoperation.PmsReservationDataLog(AzureAccountName, AzureStorageKey, AzureStorageTable, item).Result;

                   // serviceBusQueueMessage.XMLMessageUrl = item.XmlMessages;
                   // serviceBusQueueMessage.PartitionKey = item.PropertyCode.ToString();

                    //string QueueMessage = JsonConvert.SerializeObject(serviceBusQueueMessage);

                    

                    try
                    {
                        var _output = SendToGateway(xmlmessage, item.PropertyCode.ToString());

                        if(_output.MESSAGE=="Success")
                        {

                        }

                        lblReservationStatus.Text = "Reservation Transfer Process Processing....";
                        lblReservationStatus.ForeColor = Color.Green;
                        lblReservationStatus.BackColor = Color.White;

                        Thread.Sleep(5000);

                        //// Check Queue Name Exists or not 
                        //QueueExists(AzureCompanyQueue, AzureServiceBus);
                        //var _Queue = new QueueClient(AzureServiceBus, AzureCompanyQueue);
                        //var _Message = new Microsoft.Azure.ServiceBus.Message(Encoding.UTF8.GetBytes(QueueMessage));

                        //ServiceBusOperation.SASKey = "h+sGnq4sEWEd0KPL0gvcwMvvGxtAO74Jx1exFsfMEFA=";
                        //ServiceBusOperation.baseAddress = "https://SkyExchange.servicebus.windows.net/";

                        //ServiceBusOperation.SendScheduleMessage(AzureCompanyQueue, QueueMessage, "CompanyBulk");
                        //Thread.Sleep(3);

                        ////item.MessageStatus = 
                        //_outResponse = azuretableoperation.UpdatePmsCompanyDataLog(AzureAccountName, AzureStorageKey, AzureStorageTable, item).Result;


                    }
                    catch (Exception ex)
                    {
                        OutputHandler.OutputErrorResponse(ex, ref _OutputResponse);
                    }

                }
                btn_ReservationExport.Enabled = true;
                btn_ReservationExport.ForeColor = Color.Black;
                MessageBox.Show(" Data Exported Successfully !! ");
            }
            catch (Exception ex)
            {
                _Output = ex.Message.ToString();
            }
        }

        private DHResponseModel SendToGateway(string IncommingXMlMessage, string pmscode)
        {

            string url = string.Empty;
            string content = string.Empty;

            // content = new WebClient().DownloadString(IncommingXMlMessage);

            lblReservationStatus.Text = "Reservation Transfer Process Processing....";
            lblReservationStatus.ForeColor = Color.Green;
            lblReservationStatus.BackColor = Color.White;



            DHResponseModel dHResponseModel = new DHResponseModel();
            IncommingModel incomming = new IncommingModel();
            incomming.PMSCUSTCODE = Convert.ToInt64(pmscode);
            incomming.PMSCUSTID = 0;
            incomming.TOKENNUMBER = Guid.NewGuid().ToString();
            incomming.INCOMINGMESSAGE = IncommingXMlMessage; //JsonConvert.SerializeObject(IncommingXMlMessage);
            incomming.RECEIVEDDATE = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
            incomming.DATASOURCE = "DATAHUB";
            incomming.DATADESTINATION = "FXFD";
            incomming.MESSAGETYPE = "Reservation";


            if (content != null)
            {

                //incomming = JsonConvert.DeserializeObject<IncommingModel>(content);

                string _reservationUrl = "https://fxdistributionreservationgateway.azurewebsites.net/api/InReservation/ReservationData";

                var _objTojson = JsonConvert.SerializeObject(incomming);
                dHResponseModel = SharkPost(_reservationUrl, incomming);               

            }
            return dHResponseModel;

        }

        public DHResponseModel SharkPost(string Url, object model)
        {
            var _Output = new DHResponseModel();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Url);
                    var Records = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                    var response = client.PostAsync(Url, Records).Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        _Output.STATUS = true;
                        _Output.MESSAGE = response.StatusCode.ToString();
                    }
                    else
                    {
                        var responseContent = response.Content;

                        var _pb = responseContent.ReadAsStringAsync();
                        string _OutputConvertToString = _pb.ToString();
                        _Output = JsonConvert.DeserializeObject<DHResponseModel>(_OutputConvertToString);
                    }
                }


            }
            catch (Exception ex)
            {
                _Output.STATUS = false;
                _Output.MESSAGE = ex.Message.ToString();
            }
            return _Output;

        }




        private void btnClearLog_Click(object sender, EventArgs e)
        {
            rtLog.Text = "";
        }

        
    }

    public class PMSDataCompanyRequest
    {
        public string PartitionKey { get; set; } = Guid.NewGuid().ToString();
        public string RowKey { get; set; } = Guid.NewGuid().ToString();
        public long SerialNo { get; set; }
        public long PropertyCode { get; set; }
        public string ComCode { get; set; }
        public string XmlMessages { get; set; }
        public string ProcessTime { get; set; }
        public string MessageStatus { get; set; }
    }

    public class PMSDataReservationRequest
    {
        public string PartitionKey { get; set; } = Guid.NewGuid().ToString();
        public string RowKey { get; set; } = Guid.NewGuid().ToString();
        public int SerialNo { get; set; }
        public long PropertyCode { get; set; }
        public decimal ReservationNumber { get; set; }

        public string XmlMessages { get; set; }
        public string ProcessTime { get; set; }
        public string MessageStatus { get; set; }
    }



    public class ServiceBusQueueMessage
    {
        public string PartitionKey { get; set; }
        public string RowKey { get; set; } = Guid.NewGuid().ToString();
        public string XMLMessageUrl { get; set; }

    }

}
