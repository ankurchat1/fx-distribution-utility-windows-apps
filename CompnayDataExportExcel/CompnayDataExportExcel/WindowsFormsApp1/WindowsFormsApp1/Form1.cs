﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DBShark.Core.V1;
using FortuneCloud.Extensions;
using FortuneCloud.Utility;
using FXDistributionAzureStorageLog;
using Newtonsoft.Json;


namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private string _dbConnection= "Server=skyreslivedbserver.database.windows.net;Database=FXCRS_TESTDB;User ID=skyresadmin;Password=Sky@Res@123;Trusted_Connection=False";

        // private static string Excel03ConString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0 Xml;HDR=YES'";



        public static string _filePath { get; set; }
        public static string _targetSQLDb { get; set; }

        private void BrowseButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox2.Text == string.Empty)
                {
                    MessageBox.Show("Please enter a Database Name !!");
                    return;
                }                
                else
                {
                    _targetSQLDb = textBox2.Text;
                    _dbConnection = _targetSQLDb;
                    Cursor.Current = Cursors.WaitCursor;
                    //this.ConnectionStatusLabel.Text = "Connecting ...";
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString());
            }
        }
        

        private void UploadButton_Click(object sender, EventArgs e)
        {
            string _Output = string.Empty;
            DataSet DataSet = new DataSet();
            Dictionary<string, object> ResponseData = new Dictionary<string, object>();
            try
            {
                string AzureAccountName = "qaskystorage";
                string AzureStorageKey = "IVVyZxilOkGo/XdPyIsQyfqeBET80fwXEWg6d0NI7yQgEvehXKgp9jfWLNrLoCMTwfxV6J7PutWYw1Vwhw37Ew==";


                //var _DbConnection = new DBConnection();
                string _outResponse = string.Empty;

                //var _Connection = _dbConnection;
                ////var _SharkDb = new SharkDb(_Connection);
                //var _Dict = new Dictionary<string, object>();

                ////var _ResultSet = _SharkDb.ExecuteStoreProcedure("[PMS].[SP_CompnayBulkPull_XML]", _Dict).ResultSet.Tables[0];

                //Status status = (DataSet.Tables[0]).ConvertToList<Status>()[0];

                //if (status.IsSuccess == false)

                //{
                //    ResponseData = Response.ErrorResponse(status.Type, status.Code, status.Description);
                //}
                //else
                //{

                //    ResponseData = Response.SuccessResponse(status.Type, status.Code, status.Type, (DataSet.Tables[1]).ConvertToObject<PMSDataCompanyRequest>());
                //}




                //Task.Run(() =>
                //{
                //    _outResponse = AzureTableStorage.PmsCompanyDataLog(AzureAccountName, AzureStorageKey, pMSDataCompanyRequests).Result;
                //});

                
                string connetionString = null;
                SqlConnection connection;
                SqlDataAdapter adapter;
                SqlCommand command = new SqlCommand();
                DataSet ds = new DataSet();

                int i = 0;

                connetionString = _dbConnection;
                connection = new SqlConnection(connetionString);

                connection.Open();
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "[PMS].[SP_CompnayBulkPull_XML]";
                adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);

                List<PMSDataCompanyRequest> lstPmsCompany = ds.Tables[0].AsEnumerable().Select(
                            dataRow => new PMSDataCompanyRequest
                            {                                
                                SerialNo = dataRow.Field<int>("SRLNUB"),
                                PropertyCode = dataRow.Field<long>("PMSCODE"),
                                ComCode = dataRow.Field<string>("COMCOD"),
                                XmlMessages = dataRow.Field<string>("XMLMESSAGE"),
                                ProcessTime = DateTime.Now.ToString()
                            }).ToList();

                PMSDataCompanyRequest pMSDataCompanyRequest = new PMSDataCompanyRequest();

                //Task.Run(() =>
                //{
                //    _outResponse = AzureTableStorage.PmsCompanyDataLog(AzureAccountName, AzureStorageKey, pMSDataCompanyRequest).Result;
                //});


                connection.Close();
            }
            catch (Exception ex)
            {
                _Output = ex.Message.ToString();
            }
        }

        
    }

    public class PMSDataCompanyRequest
    {
        public string PartitionKey { get; set; } = Guid.NewGuid().ToString();
        public string RowKey { get; set; } =  Guid.NewGuid().ToString();
        public long SerialNo { get; set; }
        public long PropertyCode { get; set; }
        public string ComCode { get; set; }
        public string XmlMessages { get; set; }
        public string ProcessTime { get; set; }
    }

}
