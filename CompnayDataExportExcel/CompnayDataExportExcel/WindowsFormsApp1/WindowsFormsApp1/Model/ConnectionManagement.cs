﻿using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class ConnectionModel
    {
        public int ProductCode { get; set; }
        public string Connection { get; set; }
    }

    public class ServerConnectionModel
    {
        public string connectionString { get; set; }
        public int Status { get; set; }
        public string message { get; set; }
    }

    internal static class ConnectionManagement
    {
        public static ConcurrentDictionary<long, List<ConnectionModel>> DatabaseConnection = new ConcurrentDictionary<long, List<ConnectionModel>>();



        public static bool CheckExists(int ProductCode, long PropertyID)
        {
            if(DatabaseConnection.Count>0)
            {

                if(DatabaseConnection.ContainsKey(PropertyID))
                {
                    var _ConnectionBundle = DatabaseConnection[PropertyID].Where(p => p.ProductCode == ProductCode).FirstOrDefault();
                    if (_ConnectionBundle != null)
                        return true;
                    else
                        return false;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static void Add(int ProductCode, long PropertyID, string Connection, out string ConnectionString)
        {

            if (DatabaseConnection.ContainsKey(PropertyID))
            {
                var _ConnectionBundle = DatabaseConnection[PropertyID].Where(p => p.ProductCode == ProductCode).FirstOrDefault();
                var _Connection = string.Empty;
                if (_ConnectionBundle != null)
                {
                    _Connection = _ConnectionBundle.Connection;
                }
                else
                {
                    var _GetAllConnection = DatabaseConnection[PropertyID];
                    _GetAllConnection.Add(new ConnectionModel()
                    {
                        Connection = Connection,
                        ProductCode = ProductCode
                    });
                    _Connection = Connection;
                }

                ConnectionString = _Connection;
            }
            else
            {
                var _ConnectionModel = new List<ConnectionModel>();
                _ConnectionModel.Add(new ConnectionModel()
                {
                    Connection = Connection,
                    ProductCode = ProductCode
                });

                DatabaseConnection.TryAdd(PropertyID, _ConnectionModel);
                ConnectionString = Connection;
            }
        }
    }

    public class DBConnection
    {
        public async Task<string> GetPropertyConnection(long PropertyID, int ProductCode)
        {
            #region Prepare ConnectionModel  for Property Connection String  
            var _ConnectionModel = new
            {
                User = "prithu",
                PropertyID = PropertyID,
                ProductCode = ProductCode
            };

           var _ConnectionExists= ConnectionManagement.CheckExists(ProductCode, PropertyID);
            var _ConnectionString = string.Empty;

            if (_ConnectionExists)
            {
                ConnectionManagement.Add(ProductCode, PropertyID, "", out _ConnectionString);
            }
            else
            {

                var _PropertyConnectionString = await SharkPost("https://qafxauthentication.azurewebsites.net/api/Connection/DbConnection", _ConnectionModel);
                // var _PropertyConnectionString =await SharkAPI.SharkPost("https://fxauthenticationprod.azurewebsites.net/api/Connection/DbConnection", _ConnectionModel);

                var _ServerConnection = JsonConvert.DeserializeObject<ServerConnectionModel>(_PropertyConnectionString);
                if (_ServerConnection.Status == 1)
                {
                    ConnectionManagement.Add(ProductCode, PropertyID, _ServerConnection.connectionString, out _ConnectionString);
                }
            }
            #endregion
            return await Task.FromResult(_ConnectionString);
        }

        public async static Task<string> SharkPost(string Url, object model)
        {
            string _Output = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Url);
                    var Records = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(Url, Records);
                    if (!response.IsSuccessStatusCode)
                    {
                        _Output = "Status Code :" + response.StatusCode;
                    }
                    else
                    {
                        var responseContent = response.Content;
                        var _pb = await responseContent.ReadAsStringAsync();
                        string _OutputConvertToString = _pb;
                        _Output = _OutputConvertToString;
                    }
                }
            }
            catch (Exception ex)
            {

                _Output = "Error:" + ex.Message.ToString();
            }
            return await Task.FromResult(_Output);
        }
    }

    public class Status
    {
        public int ID { get; set; }
        /// <summary>
        /// Code (For Success starts with 'FCS'+4 digit and for Error 'FCE'+4 digit)
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Success/Error Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Success/Error
        /// </summary>
        public bool? IsSuccess { get; set; }

        /// <summary>
        /// Created On
        /// </summary>
        public DateTime? CreatedOn { get; set; }

        /// <summary>
        /// Modified On
        /// </summary>
        public DateTime? ModifiedOn { get; set; }

        /// <summary>
        /// Success/Failure
        /// </summary>
        public string Type { get; set; }

    }

}
