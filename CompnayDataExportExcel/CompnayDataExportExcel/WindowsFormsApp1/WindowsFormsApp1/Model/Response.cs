﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FortuneCloud.Utility
{
    public class Response
    {
        public static Dictionary<string, object> SuccessResponse<T>(string status, string statusCode, string statusDescription, T Response) where T : class
        {
            Dictionary<string, object> SuccessResponse = new Dictionary<string, object>();
            SuccessResponse.Add("Status", status);
            SuccessResponse.Add("StatusCode", statusCode);
            SuccessResponse.Add("StatusDescription", statusDescription);
            SuccessResponse.Add("Response", Response);
            return SuccessResponse;
        }
        public static Dictionary<string, object> SuccessResponse<T>(string status, string statusCode, string statusDescription)
        {
            Dictionary<string, object> SuccessResponse = new Dictionary<string, object>();
            SuccessResponse.Add("Status", status);
            SuccessResponse.Add("StatusCode", statusCode);
            SuccessResponse.Add("StatusDescription", statusDescription);
            return SuccessResponse;
        }

        public static Dictionary<string, object> SuccessResponse<T>(T Response) where T : class
        {
            Dictionary<string, object> SuccessResponse = new Dictionary<string, object>();           
            SuccessResponse.Add("Response", Response);
            return SuccessResponse;
        }

        public static Dictionary<string, object> ErrorResponse(string status, string statusCode, string statusDescription)
        {
            Dictionary<string, object> ErrorResponse = new Dictionary<string, object>();
            ErrorResponse.Add("Status", status);
            ErrorResponse.Add("StatusCode", statusCode);
            ErrorResponse.Add("StatusDescription", statusDescription);
            return ErrorResponse;
        }

        public static Dictionary<string, object> SuccessMDMResponse<T>(T Response) where T : class
        {
            Dictionary<string, object> SuccessResponse = new Dictionary<string, object>();
            SuccessResponse.Add("MakePaymentResponse", Response);
            return SuccessResponse;
        }


    }
}
