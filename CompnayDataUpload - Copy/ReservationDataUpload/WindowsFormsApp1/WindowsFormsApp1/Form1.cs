﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //private string Excel03ConString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR={1}'";

        private static string Excel03ConString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0 Xml;HDR=YES'";


        //string _filePath = string.Empty;
        public static string _filePath { get; set; }
        public static string _targetSQLDb { get; set; }

        private void BrowseButton_Click(object sender, EventArgs e)
        {


            OpenFileDialog od = new OpenFileDialog();
            od.Filter = "Excell|*.xls;*.xlsx;";
            DialogResult dr = od.ShowDialog();
            if (dr == DialogResult.Abort)
                return;
            if (dr == DialogResult.Cancel)
                return;
            textBox1.Text = od.FileName.ToString();
            _filePath = textBox1.Text;

        }

        private void UploadButton_Click(object sender, EventArgs e)
        {
            try
            {

                if (textBox1.Text == string.Empty)
                {
                    MessageBox.Show("Please enter a value to Excel File !!");
                    return;
                }
                //else if (textBox2.Text == string.Empty)
                //{
                //    MessageBox.Show("Please enter a Database Name !!");
                //    return;
                //}
                else
                {
                    _targetSQLDb = textBox2.Text;
                    DataTable data = RetrieveSourceData();
                    // CopyData(data);
                    MessageBox.Show("File imported into sql server.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }





        public static DataTable RetrieveSourceData()
        {
            string sourceConnString = string.Format(Excel03ConString, _filePath, "YES");
            DataTable sourceData = new DataTable();
            using (OleDbConnection conn = new OleDbConnection(sourceConnString))
            {
                conn.Open();
                // Get the data from the source table as a SqlDataReader.
                OleDbCommand command = new OleDbCommand(
                                    @"SELECT [IncommingMessage] 
                                     FROM [ReservationData$]", conn);
                DataColumn dataColumn = new DataColumn();
                OleDbDataAdapter adapter = new OleDbDataAdapter(command);
                adapter.Fill(sourceData);
                conn.Close();
            }

            int count = 0;

            foreach (DataRow dr in sourceData.Rows)
            {
                string url = string.Empty;
                string content = string.Empty;
                IncommingModel incomming = new IncommingModel();

                url = Convert.ToString(dr["Incommingmessage"]);
                content = new WebClient().DownloadString(url);
                count++;
                if (content != null)
                {                  

                    incomming = JsonConvert.DeserializeObject<IncommingModel>(content);  

                    string _reservationUrl = "https://fxdistributionreservationgateway.azurewebsites.net/api/InReservation/ReservationData";

                    var _objTojson = JsonConvert.SerializeObject(incomming);
                    SharkPost(_reservationUrl, incomming);  
                }

            }

            MessageBox.Show("Proccessed message : " + count.ToString());

            return sourceData;

        }

        public static void SharkPost(string Url, object model)
        {
            var _Output = new DHResponseModel();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Url);
                    var Records = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                    var response = client.PostAsync(Url, Records).Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        _Output.STATUS = true;
                        _Output.MESSAGE = response.StatusCode.ToString();
                    }
                    else
                    {
                        var responseContent = response.Content;

                        //_Output = _OutputConvertToString;
                    }
                }
            }
            catch (Exception ex)
            {
                _Output.STATUS = false;
                _Output.MESSAGE = ex.Message.ToString();
            }

        }
    }


    public class IncommingModel
    {
        /// <summary>
        /// Customer id ,which is unique
        /// </summary>
        public long PMSCUSTCODE { get; set; }
        /// <summary>
        /// Fxcrs id , whcih is unique created for the property in Fxcrs
        /// </summary>
        public long PMSCUSTID { get; set; }
        /// <summary>
        /// From where the message has generated
        /// </summary>
        public string DATASOURCE { get; set; }
        /// <summary>
        /// Where to send the message FXFD/Datahub or other interface
        /// </summary>
        public string DATADESTINATION { get; set; }
        /// <summary>
        /// Created Date
        /// </summary>
        public string RECEIVEDDATE { get; set; }
        /// <summary>
        /// What kind of incomming message ex:Reservation/Rate etc
        /// </summary>
        public string MESSAGETYPE { get; set; }
        /// <summary>
        /// Unique Identitification 
        /// </summary>
        public string TOKENNUMBER { get; set; }
        /// <summary>
        /// Bundle of Data XML OR JSON OR any string 
        /// </summary>
        public string INCOMINGMESSAGE { get; set; }
        /// <summary>
        /// Create date once the
        /// </summary>
        public string CREATEDDATE { get; set; }

        public string CHANNELTYPE { get; set; }

        public IncommingModel()
        {
            CREATEDDATE = DateTime.Now.AddHours(5).AddDays(30).ToString("yyyy-MM-ddTHH:mm:ss.fff");
        }

    }

    public class DHResponseModel
    {
        public bool STATUS { get; set; }
        public string MESSAGE { get; set; }
    }
    public class DistGatewayResReq
    {
        public long PMSCUSTCODE { get; set; }
        public long PMSCUSTID { get; set; }
        public string DATASOURCE { get; set; }
        public string DATADESTINATION { get; set; }
        public string RECEIVEDDATE { get; set; }
        public string MESSAGETYPE { get; set; }
        public string TOKENNUMBER { get; set; }
        public string INCOMINGMESSAGE { get; set; }
        public DateTime CREATEDDATE { get; set; }
        public string CHANNELTYPE { get; set; }
    }

    public class DistGatewayResResponse
    {
        public string PMSRESNUB { get; set; }
        public string CRSRESNUB { get; set; }
        public string CUSTCOD { get; set; }
        public string PARTNERRES { get; set; }
        public string TRANSSTATUS { get; set; }
    }
}
