﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DistributionConfiguration.BusinessObject;

namespace DistributionConfiguration
{
    public partial class Frm1 : Form
    {
        public Frm1()
        {
            InitializeComponent();
        }

        private void Frm1_Load(object sender, EventArgs e)
        {
            textBox1.Text =  CommonMethod.GetMacAddress().ToString();

            textBox2.Text = CommonMethod.GetIPAddress(Dns.GetHostName()).ToString();

            textBox3.Text = CommonMethod.GetIPAddress("google.com").ToString();
        }
    }
}
