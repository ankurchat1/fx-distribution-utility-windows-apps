﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DistributionConfiguration
{
    public partial class FrmConnectDB : Form
    {
        public FrmConnectDB()
        {
            InitializeComponent();
        }

        private string FullPathDB = string.Empty;
        private string _databaseNameQA = "Server=skyreslivedbserver.database.windows.net;Database=FXCRS_TESTDB;User ID=skyresadmin;Password=Sky@Res@123;Trusted_Connection=False";
        private string _databaseNameProduction = "Server=fxoneadmin.database.windows.net;Database=FX_Sabre_Interface_Log;User ID=fxoneadmin;Password=FXOneAdm@123;Trusted_Connection=False";
        private string _connectionType = string.Empty;

        private void btnConnectQA_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToDecimal(DateTime.Now.ToString("yyyyMMdd")) >  20200831)
                {
                    MessageBox.Show("Application Is Expired !!");
                    this.Close();
                    return;
                } 


                if (!IsServerConnected(_databaseNameQA))
                {
                    MessageBox.Show("Due to Some network Issue Server Not Connected !! ");
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Server Connected !! ");
                    FullPathDB = _databaseNameQA;
                    _connectionType = "QA";
                    FrmDistributionConfigMain obj1 = new FrmDistributionConfigMain(FullPathDB, _connectionType);
                    obj1.Show();
                    this.Hide();
                }
                             
                

                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void btnConnectProd_Click(object sender, EventArgs e)
        {
            try
            {

                if (Convert.ToDecimal(DateTime.Now.ToString("yyyyMMdd")) > 20200831)
                {
                    MessageBox.Show("Application Is Expired !!");
                    this.Close();
                    return;
                }

                if (!IsServerConnected(_databaseNameProduction))
                {
                    MessageBox.Show("Due to Some network Issue Server Not Connected !! ");
                    this.Hide();
                }
                else
                {

                    MessageBox.Show("Server Connected !! ");
                    FullPathDB = _databaseNameProduction;
                    _connectionType = "Production";
                    FrmDistributionConfigMain obj1 = new FrmDistributionConfigMain(FullPathDB, _connectionType);
                    obj1.Show();
                    this.Hide();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
           
        }

        private static bool IsServerConnected(string connectionString)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    return true;
                }
                catch (SqlException ex)
                {
                    return false;
                }
            }
        }

        private void FrmConnectDB_Load(object sender, EventArgs e)
        {

        }
    }
}
