﻿namespace DistributionConfiguration
{
    partial class FrmDistributionConfigMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPmsCustCode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPropertyId = new System.Windows.Forms.Label();
            this.txtPropertyId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPropertyCode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtGroupCode = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.chkIsEnabled = new System.Windows.Forms.CheckBox();
            this.cmbSavePriority = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnChannalSave = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.chkIsFOMEnaled = new System.Windows.Forms.CheckBox();
            this.chkIsCRSEnabled = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.UploadButton = new System.Windows.Forms.Button();
            this.txtBrowse = new System.Windows.Forms.TextBox();
            this.BrowseButton = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.chkDHGuest = new System.Windows.Forms.CheckBox();
            this.chkDHCompany = new System.Windows.Forms.CheckBox();
            this.chkDHHotelPosition = new System.Windows.Forms.CheckBox();
            this.chkDHReservation = new System.Windows.Forms.CheckBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.chkCRSGroupBlock = new System.Windows.Forms.CheckBox();
            this.chkCRSRateRestriction = new System.Windows.Forms.CheckBox();
            this.chkCRSRate = new System.Windows.Forms.CheckBox();
            this.chkCRSGuest = new System.Windows.Forms.CheckBox();
            this.chkCRSCompany = new System.Windows.Forms.CheckBox();
            this.chkCRSReservation = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtPmsCustCode
            // 
            this.txtPmsCustCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPmsCustCode.Location = new System.Drawing.Point(253, 106);
            this.txtPmsCustCode.MaxLength = 8;
            this.txtPmsCustCode.Name = "txtPmsCustCode";
            this.txtPmsCustCode.ReadOnly = true;
            this.txtPmsCustCode.Size = new System.Drawing.Size(330, 22);
            this.txtPmsCustCode.TabIndex = 24;
            this.txtPmsCustCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPmsCustCode_KeyDown);
            this.txtPmsCustCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPmsCustCode_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(32, 109);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 16);
            this.label1.TabIndex = 25;
            this.label1.Text = "Enter PMS Customer Code :";
            // 
            // lblPropertyId
            // 
            this.lblPropertyId.AutoSize = true;
            this.lblPropertyId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPropertyId.Location = new System.Drawing.Point(32, 152);
            this.lblPropertyId.Name = "lblPropertyId";
            this.lblPropertyId.Size = new System.Drawing.Size(115, 16);
            this.lblPropertyId.TabIndex = 27;
            this.lblPropertyId.Text = "Enter Property ID :";
            // 
            // txtPropertyId
            // 
            this.txtPropertyId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPropertyId.Location = new System.Drawing.Point(253, 149);
            this.txtPropertyId.MaxLength = 8;
            this.txtPropertyId.Name = "txtPropertyId";
            this.txtPropertyId.ReadOnly = true;
            this.txtPropertyId.Size = new System.Drawing.Size(330, 22);
            this.txtPropertyId.TabIndex = 26;
            this.txtPropertyId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPropertyId_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(32, 198);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 16);
            this.label2.TabIndex = 29;
            this.label2.Text = "Enter Property Code :";
            // 
            // txtPropertyCode
            // 
            this.txtPropertyCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPropertyCode.Location = new System.Drawing.Point(253, 195);
            this.txtPropertyCode.MaxLength = 8;
            this.txtPropertyCode.Name = "txtPropertyCode";
            this.txtPropertyCode.ReadOnly = true;
            this.txtPropertyCode.Size = new System.Drawing.Size(330, 22);
            this.txtPropertyCode.TabIndex = 28;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(32, 248);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 16);
            this.label3.TabIndex = 31;
            this.label3.Text = "Enter Group Code :";
            // 
            // txtGroupCode
            // 
            this.txtGroupCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGroupCode.Location = new System.Drawing.Point(253, 245);
            this.txtGroupCode.MaxLength = 8;
            this.txtGroupCode.Name = "txtGroupCode";
            this.txtGroupCode.ReadOnly = true;
            this.txtGroupCode.Size = new System.Drawing.Size(330, 22);
            this.txtGroupCode.TabIndex = 30;
            this.txtGroupCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGroupCode_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(32, 311);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(124, 16);
            this.label5.TabIndex = 34;
            this.label5.Text = "Enter Save Priority :";
            // 
            // chkIsEnabled
            // 
            this.chkIsEnabled.AutoSize = true;
            this.chkIsEnabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIsEnabled.Location = new System.Drawing.Point(35, 348);
            this.chkIsEnabled.Name = "chkIsEnabled";
            this.chkIsEnabled.Size = new System.Drawing.Size(237, 20);
            this.chkIsEnabled.TabIndex = 46;
            this.chkIsEnabled.Text = "Is Enabled  (Inventory Send To DH)";
            this.chkIsEnabled.UseVisualStyleBackColor = true;
            // 
            // cmbSavePriority
            // 
            this.cmbSavePriority.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSavePriority.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSavePriority.FormattingEnabled = true;
            this.cmbSavePriority.Items.AddRange(new object[] {
            "SELECT",
            "FXCRS",
            "FOM"});
            this.cmbSavePriority.Location = new System.Drawing.Point(253, 304);
            this.cmbSavePriority.Name = "cmbSavePriority";
            this.cmbSavePriority.Size = new System.Drawing.Size(183, 23);
            this.cmbSavePriority.TabIndex = 55;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(462, 450);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(101, 35);
            this.btnCancel.TabIndex = 57;
            this.btnCancel.Text = "Exit";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnChannalSave
            // 
            this.btnChannalSave.Location = new System.Drawing.Point(355, 450);
            this.btnChannalSave.Name = "btnChannalSave";
            this.btnChannalSave.Size = new System.Drawing.Size(101, 35);
            this.btnChannalSave.TabIndex = 56;
            this.btnChannalSave.Text = "Save";
            this.btnChannalSave.UseVisualStyleBackColor = true;
            this.btnChannalSave.Click += new System.EventHandler(this.btnChannalSave_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(569, 450);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(103, 35);
            this.button1.TabIndex = 58;
            this.button1.Text = "Refresh Chache";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // chkIsFOMEnaled
            // 
            this.chkIsFOMEnaled.AutoSize = true;
            this.chkIsFOMEnaled.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIsFOMEnaled.Location = new System.Drawing.Point(459, 333);
            this.chkIsFOMEnaled.Name = "chkIsFOMEnaled";
            this.chkIsFOMEnaled.Size = new System.Drawing.Size(126, 20);
            this.chkIsFOMEnaled.TabIndex = 60;
            this.chkIsFOMEnaled.Text = "Is Enabled  FOM";
            this.chkIsFOMEnaled.UseVisualStyleBackColor = true;
            // 
            // chkIsCRSEnabled
            // 
            this.chkIsCRSEnabled.AutoSize = true;
            this.chkIsCRSEnabled.Checked = true;
            this.chkIsCRSEnabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsCRSEnabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIsCRSEnabled.Location = new System.Drawing.Point(459, 307);
            this.chkIsCRSEnabled.Name = "chkIsCRSEnabled";
            this.chkIsCRSEnabled.Size = new System.Drawing.Size(141, 20);
            this.chkIsCRSEnabled.TabIndex = 61;
            this.chkIsCRSEnabled.Text = "Is Enabled  FXCRS";
            this.chkIsCRSEnabled.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 15);
            this.label4.TabIndex = 66;
            this.label4.Text = "Source Excel File";
            // 
            // UploadButton
            // 
            this.UploadButton.Location = new System.Drawing.Point(475, 27);
            this.UploadButton.Name = "UploadButton";
            this.UploadButton.Size = new System.Drawing.Size(89, 33);
            this.UploadButton.TabIndex = 64;
            this.UploadButton.Text = "Upload";
            this.UploadButton.UseVisualStyleBackColor = true;
            this.UploadButton.Click += new System.EventHandler(this.UploadButton_Click);
            // 
            // txtBrowse
            // 
            this.txtBrowse.Location = new System.Drawing.Point(114, 27);
            this.txtBrowse.Multiline = true;
            this.txtBrowse.Name = "txtBrowse";
            this.txtBrowse.Size = new System.Drawing.Size(260, 34);
            this.txtBrowse.TabIndex = 63;
            // 
            // BrowseButton
            // 
            this.BrowseButton.Location = new System.Drawing.Point(380, 27);
            this.BrowseButton.Name = "BrowseButton";
            this.BrowseButton.Size = new System.Drawing.Size(89, 35);
            this.BrowseButton.TabIndex = 62;
            this.BrowseButton.Text = "Browse";
            this.BrowseButton.UseVisualStyleBackColor = true;
            this.BrowseButton.Click += new System.EventHandler(this.BrowseButton_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkBox8);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.BrowseButton);
            this.groupBox3.Controls.Add(this.UploadButton);
            this.groupBox3.Controls.Add(this.txtBrowse);
            this.groupBox3.Location = new System.Drawing.Point(16, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(584, 79);
            this.groupBox3.TabIndex = 67;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Parameters  Configuration (Excel Upload)";
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(772, 38);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(73, 17);
            this.checkBox8.TabIndex = 6;
            this.checkBox8.Text = "Meal Plan";
            this.checkBox8.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tabControl1);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Location = new System.Drawing.Point(629, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(354, 399);
            this.groupBox1.TabIndex = 68;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Channel - Interface API Confiration";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(17, 27);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(324, 366);
            this.tabControl1.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.chkDHGuest);
            this.tabPage1.Controls.Add(this.chkDHCompany);
            this.tabPage1.Controls.Add(this.chkDHHotelPosition);
            this.tabPage1.Controls.Add(this.chkDHReservation);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(316, 340);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "DATAHUB";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // chkDHGuest
            // 
            this.chkDHGuest.AutoSize = true;
            this.chkDHGuest.Checked = true;
            this.chkDHGuest.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDHGuest.Location = new System.Drawing.Point(20, 129);
            this.chkDHGuest.Name = "chkDHGuest";
            this.chkDHGuest.Size = new System.Drawing.Size(54, 17);
            this.chkDHGuest.TabIndex = 3;
            this.chkDHGuest.Text = "Guest";
            this.chkDHGuest.UseVisualStyleBackColor = true;
            // 
            // chkDHCompany
            // 
            this.chkDHCompany.AutoSize = true;
            this.chkDHCompany.Checked = true;
            this.chkDHCompany.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDHCompany.Location = new System.Drawing.Point(20, 93);
            this.chkDHCompany.Name = "chkDHCompany";
            this.chkDHCompany.Size = new System.Drawing.Size(70, 17);
            this.chkDHCompany.TabIndex = 2;
            this.chkDHCompany.Text = "Company";
            this.chkDHCompany.UseVisualStyleBackColor = true;
            // 
            // chkDHHotelPosition
            // 
            this.chkDHHotelPosition.AutoSize = true;
            this.chkDHHotelPosition.Checked = true;
            this.chkDHHotelPosition.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDHHotelPosition.Location = new System.Drawing.Point(20, 58);
            this.chkDHHotelPosition.Name = "chkDHHotelPosition";
            this.chkDHHotelPosition.Size = new System.Drawing.Size(91, 17);
            this.chkDHHotelPosition.TabIndex = 1;
            this.chkDHHotelPosition.Text = "Hotel Position";
            this.chkDHHotelPosition.UseVisualStyleBackColor = true;
            // 
            // chkDHReservation
            // 
            this.chkDHReservation.AutoSize = true;
            this.chkDHReservation.Checked = true;
            this.chkDHReservation.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDHReservation.Location = new System.Drawing.Point(20, 21);
            this.chkDHReservation.Name = "chkDHReservation";
            this.chkDHReservation.Size = new System.Drawing.Size(83, 17);
            this.chkDHReservation.TabIndex = 0;
            this.chkDHReservation.Text = "Reservation";
            this.chkDHReservation.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.chkCRSGroupBlock);
            this.tabPage2.Controls.Add(this.chkCRSRateRestriction);
            this.tabPage2.Controls.Add(this.chkCRSRate);
            this.tabPage2.Controls.Add(this.chkCRSGuest);
            this.tabPage2.Controls.Add(this.chkCRSCompany);
            this.tabPage2.Controls.Add(this.chkCRSReservation);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(316, 340);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "FX-CRS";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // chkCRSGroupBlock
            // 
            this.chkCRSGroupBlock.AutoSize = true;
            this.chkCRSGroupBlock.Checked = true;
            this.chkCRSGroupBlock.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCRSGroupBlock.Location = new System.Drawing.Point(18, 213);
            this.chkCRSGroupBlock.Name = "chkCRSGroupBlock";
            this.chkCRSGroupBlock.Size = new System.Drawing.Size(85, 17);
            this.chkCRSGroupBlock.TabIndex = 13;
            this.chkCRSGroupBlock.Text = "Group Block";
            this.chkCRSGroupBlock.UseVisualStyleBackColor = true;
            // 
            // chkCRSRateRestriction
            // 
            this.chkCRSRateRestriction.AutoSize = true;
            this.chkCRSRateRestriction.Checked = true;
            this.chkCRSRateRestriction.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCRSRateRestriction.Location = new System.Drawing.Point(18, 177);
            this.chkCRSRateRestriction.Name = "chkCRSRateRestriction";
            this.chkCRSRateRestriction.Size = new System.Drawing.Size(102, 17);
            this.chkCRSRateRestriction.TabIndex = 12;
            this.chkCRSRateRestriction.Text = "Rate Restriction";
            this.chkCRSRateRestriction.UseVisualStyleBackColor = true;
            // 
            // chkCRSRate
            // 
            this.chkCRSRate.AutoSize = true;
            this.chkCRSRate.Checked = true;
            this.chkCRSRate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCRSRate.Location = new System.Drawing.Point(18, 137);
            this.chkCRSRate.Name = "chkCRSRate";
            this.chkCRSRate.Size = new System.Drawing.Size(49, 17);
            this.chkCRSRate.TabIndex = 11;
            this.chkCRSRate.Text = "Rate";
            this.chkCRSRate.UseVisualStyleBackColor = true;
            // 
            // chkCRSGuest
            // 
            this.chkCRSGuest.AutoSize = true;
            this.chkCRSGuest.Checked = true;
            this.chkCRSGuest.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCRSGuest.Location = new System.Drawing.Point(18, 96);
            this.chkCRSGuest.Name = "chkCRSGuest";
            this.chkCRSGuest.Size = new System.Drawing.Size(54, 17);
            this.chkCRSGuest.TabIndex = 10;
            this.chkCRSGuest.Text = "Guest";
            this.chkCRSGuest.UseVisualStyleBackColor = true;
            // 
            // chkCRSCompany
            // 
            this.chkCRSCompany.AutoSize = true;
            this.chkCRSCompany.Checked = true;
            this.chkCRSCompany.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCRSCompany.Location = new System.Drawing.Point(18, 56);
            this.chkCRSCompany.Name = "chkCRSCompany";
            this.chkCRSCompany.Size = new System.Drawing.Size(70, 17);
            this.chkCRSCompany.TabIndex = 9;
            this.chkCRSCompany.Text = "Company";
            this.chkCRSCompany.UseVisualStyleBackColor = true;
            // 
            // chkCRSReservation
            // 
            this.chkCRSReservation.AutoSize = true;
            this.chkCRSReservation.Checked = true;
            this.chkCRSReservation.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCRSReservation.Location = new System.Drawing.Point(18, 19);
            this.chkCRSReservation.Name = "chkCRSReservation";
            this.chkCRSReservation.Size = new System.Drawing.Size(83, 17);
            this.chkCRSReservation.TabIndex = 7;
            this.chkCRSReservation.Text = "Reservation";
            this.chkCRSReservation.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(772, 38);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(73, 17);
            this.checkBox1.TabIndex = 6;
            this.checkBox1.Text = "Meal Plan";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // FrmDistributionConfigMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1004, 514);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.chkIsCRSEnabled);
            this.Controls.Add(this.chkIsFOMEnaled);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnChannalSave);
            this.Controls.Add(this.cmbSavePriority);
            this.Controls.Add(this.chkIsEnabled);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtGroupCode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPropertyCode);
            this.Controls.Add(this.lblPropertyId);
            this.Controls.Add(this.txtPropertyId);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPmsCustCode);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmDistributionConfigMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Distribution Config";
            this.Load += new System.EventHandler(this.FrmDistributionConfigMain_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtPmsCustCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPropertyId;
        private System.Windows.Forms.TextBox txtPropertyId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPropertyCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtGroupCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkIsEnabled;
        private System.Windows.Forms.ComboBox cmbSavePriority;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnChannalSave;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox chkIsFOMEnaled;
        private System.Windows.Forms.CheckBox chkIsCRSEnabled;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button UploadButton;
        private System.Windows.Forms.TextBox txtBrowse;
        private System.Windows.Forms.Button BrowseButton;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.CheckBox chkDHGuest;
        private System.Windows.Forms.CheckBox chkDHCompany;
        private System.Windows.Forms.CheckBox chkDHHotelPosition;
        private System.Windows.Forms.CheckBox chkDHReservation;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.CheckBox chkCRSGroupBlock;
        private System.Windows.Forms.CheckBox chkCRSRateRestriction;
        private System.Windows.Forms.CheckBox chkCRSRate;
        private System.Windows.Forms.CheckBox chkCRSGuest;
        private System.Windows.Forms.CheckBox chkCRSCompany;
        private System.Windows.Forms.CheckBox chkCRSReservation;
    }
}