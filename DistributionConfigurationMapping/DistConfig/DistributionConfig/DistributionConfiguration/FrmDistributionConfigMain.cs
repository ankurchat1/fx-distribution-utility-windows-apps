﻿using DistributionConfiguration.BusinessObject;
using DistributionConfiguration.BusinessObject.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DistributionConfiguration
{
    public partial class FrmDistributionConfigMain : Form
    {
        public static string _dbConnection = string.Empty;
        public static string _dbConnectionType = string.Empty;
        //private static string Excel03ConString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0 Xml;HDR=YES'";
        public static string FullPathDB = string.Empty;
        public static string _SET_PMSCustCode = "";
        public static string _SET_PropertyID = "";
        public static string _SETGroupCode = "";
        public static string _filePath { get; set; }
        public static string _targetSQLDb { get; set; }


        public FrmDistributionConfigMain()
        {
            InitializeComponent();
        }



        public FrmDistributionConfigMain(string _connecttion)
        {
            _dbConnection = _connecttion;
            InitializeComponent();
        }

        public FrmDistributionConfigMain(string _connecttion,string _connectionType)
        {
            _dbConnection = _connecttion;
            _dbConnectionType = _connectionType;
            InitializeComponent();
        }

        public FrmDistributionConfigMain(string _connecttion, string _pmsCustCode, string _propertyId,string _grpCode)
        {
            _dbConnection = _connecttion;
            _SET_PMSCustCode = _pmsCustCode;
            _SET_PropertyID = _propertyId;
            _SETGroupCode = _grpCode;




            InitializeComponent();
        }


        private void btnParameterConfig_Click(object sender, EventArgs e)
        {
            FullPathDB = _dbConnection;

            _SET_PMSCustCode = txtPmsCustCode.Text.ToString();
            _SET_PropertyID = txtPropertyId.Text.ToString();
            _SETGroupCode = txtGroupCode.Text.ToString();


            //ParameterConfigForm obj1 = new ParameterConfigForm(FullPathDB, _SET_PMSCustCode, _SET_PropertyID,_SETGroupCode);
            //obj1.Show();
            this.Hide();
        }

        private void btnFXCRSConfig_Click(object sender, EventArgs e)
        {
            FullPathDB = _dbConnection;
            //FxCRSConfig obj1 = new FxCRSConfig(FullPathDB);
            //obj1.Show();
            this.Hide();
        }

        private void FxOnePropertyConfig_Click(object sender, EventArgs e)
        {
            FullPathDB = _dbConnection;
            //FxOnePropertyConfig obj1 = new FxOnePropertyConfig(FullPathDB);
            //obj1.Show();
            this.Hide();
        }

        private void btnInterfaceConfig_Click(object sender, EventArgs e)
        {
            FullPathDB = _dbConnection;

            _SET_PMSCustCode = txtPmsCustCode.Text.ToString();
            _SET_PropertyID = txtPropertyId.Text.ToString();
            _SETGroupCode = txtGroupCode.Text.ToString();

            //ChannelInterfaceConfig obj1 = new ChannelInterfaceConfig(FullPathDB, _SET_PMSCustCode, _SET_PropertyID,_SETGroupCode);            
            //obj1.Show();
            this.Hide();
        }

        private void FrmDistributionConfigMain_Load(object sender, EventArgs e)
        {
            LoadControl();
        }

        public void LoadControl()
        {
            //SETFireWallEnable();
           //// bool checkAuth = CheckAuthAccess();
           // if (checkAuth == false)
           // {
           //     MessageBox.Show("Sorry !! your machine is not authorized for Channel Configration, Please contact to Administrtor");
           //     this.Close();
           // }
           // else 
           // {
                cmbSavePriority.SelectedIndex = 1;
                chkIsEnabled.Checked = false;
                txtPmsCustCode.Text = _SET_PMSCustCode.ToString();
                txtPropertyId.Text = _SET_PropertyID.ToString();
                txtGroupCode.Text = _SETGroupCode.ToString();

                chkDHReservation.Checked = true;
                chkDHHotelPosition.Checked = true;
                chkCRSReservation.Checked = true;
            //}
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            //FullPathDB = _dbConnection;

            //_SET_PMSCustCode = txtPmsCustCode.Text.ToString();
            //_SET_PropertyID = txtPropertyId.Text.ToString();

            //FrmDistributionConfigMain obj1 = new FrmDistributionConfigMain(FullPathDB);
            //obj1.Show();
            this.Close();
        }

        private void btnChannalSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPmsCustCode.Text == string.Empty)
                {
                    MessageBox.Show("Please enter PMS Customer Code !!");
                    return;
                }
                else if (txtPropertyId.Text == string.Empty)
                {
                    MessageBox.Show("Please enter Property ID !!");
                    return;
                }
                else if (txtPropertyCode.Text == string.Empty)
                {
                    MessageBox.Show("Please enter Property Code !!");
                    return;
                }
                else if (txtGroupCode.Text == string.Empty)
                {
                    MessageBox.Show("Please enter Group Code !!");
                    return;
                }
                //else if (txtProductCode.Text == string.Empty)
                //{
                //    MessageBox.Show("Please enter Product Code  !!");
                //    return;
                //}
                else if (cmbSavePriority.SelectedIndex == 0)
                {
                    MessageBox.Show("Please Select the Save Priority !!");
                    return;
                }
                else
                {                    

                    FxCrsMappingSetting _crsMapper = new FxCrsMappingSetting();                    
                    _crsMapper.PmsCustCode = Int64.Parse(txtPmsCustCode.Text.ToString());                    
                    _crsMapper.FxCrsId = Int64.Parse(txtPropertyId.Text.ToString());
                    _crsMapper.PropertyCode = txtPropertyCode.Text.ToString();
                    if (chkIsEnabled.Checked == true)
                        _crsMapper.IsDHInventoryEnable = true;
                    else
                        _crsMapper.IsDHInventoryEnable = false;
                    _crsMapper.ModifyDatetime = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));
                    _crsMapper.UserId = "interface@idsnext.com";
                    _crsMapper.IsActive = true;

                    using (SqlConnection con = new SqlConnection(_dbConnection))
                    {
                        string query = "insert into FxCrsMapping values (@ID,@Pmscustcode,@FxCrsId,@PropertyCode,@IsDHInventoryEnable,@IsCMConfigure,@ModifiedDateTime,@UserId,@IsActive)";
                        using (SqlCommand cmd = new SqlCommand(query))
                        {
                            cmd.Connection = con;
                            cmd.Parameters.AddWithValue("@ID", _crsMapper.ID);
                            cmd.Parameters.AddWithValue("@Pmscustcode", _crsMapper.PmsCustCode);
                            cmd.Parameters.AddWithValue("@FxCrsId", _crsMapper.FxCrsId);
                            cmd.Parameters.AddWithValue("@PropertyCode", _crsMapper.PropertyCode);
                            cmd.Parameters.AddWithValue("@IsDHInventoryEnable", _crsMapper.IsDHInventoryEnable);
                            cmd.Parameters.AddWithValue("@IsCMConfigure", false);
                            cmd.Parameters.AddWithValue("@ModifiedDateTime", _crsMapper.ModifyDatetime);
                            cmd.Parameters.AddWithValue("@UserId", _crsMapper.UserId);
                            cmd.Parameters.AddWithValue("@IsActive", _crsMapper.IsActive);

                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    FxOnePropertySetting _fx1PropertyMapper = new FxOnePropertySetting();

                    _fx1PropertyMapper.PmsCustCode = Int64.Parse(txtPmsCustCode.Text.ToString());
                    _fx1PropertyMapper.PropertyID = Int64.Parse(txtPropertyId.Text.ToString());
                    _fx1PropertyMapper.GroupCode = Int64.Parse(txtGroupCode.Text.ToString());
                    _fx1PropertyMapper.ProductCode = 10001; //Int64.Parse(txtProductCode.Text.ToString());
                    _fx1PropertyMapper.SavePriority = cmbSavePriority.SelectedItem.ToString();

                    if (chkIsCRSEnabled.Checked == true)
                        _fx1PropertyMapper.IsFXCRSEnabled = true;
                    else
                    {
                        _fx1PropertyMapper.IsFXCRSEnabled = false;
                    }

                    if (chkIsFOMEnaled.Checked == true)
                        _fx1PropertyMapper.IsFXFOMEnabled = true;
                    else {
                        _fx1PropertyMapper.IsFXFOMEnabled = false;
                    }
                    if (cmbSavePriority.SelectedItem.ToString() == "FOM")
                    {
                        _fx1PropertyMapper.IsFXFOMEnabled = true;
                        _fx1PropertyMapper.IsFXCRSEnabled = false;

                    }
                    if (cmbSavePriority.SelectedItem.ToString() == "FXCRS")
                    {
                        _fx1PropertyMapper.IsFXFOMEnabled = false;
                        _fx1PropertyMapper.IsFXCRSEnabled = true;
                    }

                    _fx1PropertyMapper.ModifyDatetime = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));
                    _fx1PropertyMapper.UserID = "interface@idsnext.com";


                    using (SqlConnection con = new SqlConnection(_dbConnection))
                    {
                        string query = "insert into FxDist_FxOnePropertyDetails values (@PropertyID,@PmsCustCode,@GroupCode,@ProductCode,@IsFXCRSEnabled,@IsFXFOMEnabled,@SavePriority,@UserId,@ModifiedDateTime)";
                        using (SqlCommand cmd = new SqlCommand(query))
                        {
                            cmd.Connection = con;
                            cmd.Parameters.AddWithValue("@PropertyID", _fx1PropertyMapper.PropertyID);
                            cmd.Parameters.AddWithValue("@PmsCustCode", _fx1PropertyMapper.PmsCustCode);
                            cmd.Parameters.AddWithValue("@GroupCode", _fx1PropertyMapper.GroupCode);
                            cmd.Parameters.AddWithValue("@ProductCode", _fx1PropertyMapper.ProductCode);
                            cmd.Parameters.AddWithValue("@IsFXCRSEnabled", _fx1PropertyMapper.IsFXCRSEnabled);
                            cmd.Parameters.AddWithValue("@IsFXFOMEnabled", _fx1PropertyMapper.IsFXFOMEnabled);
                            cmd.Parameters.AddWithValue("@SavePriority", _fx1PropertyMapper.SavePriority);
                            cmd.Parameters.AddWithValue("@UserId", _fx1PropertyMapper.UserID);
                            cmd.Parameters.AddWithValue("@ModifiedDateTime", _fx1PropertyMapper.ModifyDatetime);


                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                    MessageBox.Show("FX1 Property Configuration-CRS Mapping Saved !! ");
                    UnloadControl();




                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                //ex.Message.ToString();
            }
        }

        private void UnloadControl()
        {
            txtPmsCustCode.Text = "";
            txtPropertyId.Text = "";
            txtPropertyCode.Text = "";
            txtGroupCode.Text = "";
           // txtProductCode.Text = "";
        }

        private void txtPmsCustCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            
        }

        private void txtPropertyId_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtGroupCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtProductCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }        


        private MasterDataRequest GetMasterDatafromDB(string _PmsCustCode)
        {
            MasterDataRequest master = new MasterDataRequest();
            string _fxOneConnecttion = "Server=fxoneadmin.database.windows.net;Database=FXOneAdmin;User ID=fxoneadmin;Password=FXOneAdm@123;Trusted_Connection=False";


            string SqlString = "SELECT ID,PMSCode,GroupCode FROM tbl_Property where PMSCode="+ _PmsCustCode;
            using (SqlConnection conn = new SqlConnection(_fxOneConnecttion))
            {
                using (SqlCommand cmd = new SqlCommand(SqlString, conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("PMSCode", _PmsCustCode);

                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {                        
                        {
                            while (reader.Read())
                            {
                                master.PropertyId = reader["ID"].ToString();
                                master.PmsCustCode = reader["PMSCode"].ToString();
                                master.GroupCode = reader["GroupCode"].ToString();
                            }
                        }                       

                        reader.Close();
                    }                   
                }
                conn.Close();
            }

            return master;
        }


        private GetMACAddress GetMACAdress(string _macRequest)
        {
            GetMACAddress master = new GetMACAddress();
            
            string SqlString = "SELECT ID, MACAddress,IPAddress,IsActive FROM FXDistMACAddress where MACAddress='" + _macRequest+"'";
            using (SqlConnection conn = new SqlConnection(_dbConnection))
            {
                using (SqlCommand cmd = new SqlCommand(SqlString, conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("MACAddress", _macRequest);

                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        {
                            while (reader.Read())
                            {
                                master.ID = Convert.ToInt64(reader["ID"].ToString());
                                master.MACAddress = reader["MACAddress"].ToString();
                                master.IPAddress = reader["IPAddress"].ToString();
                                master.IsActive = Convert.ToBoolean(reader["IsActive"].ToString());
                            }
                        }

                        reader.Close();
                    }
                }
                conn.Close();
            }

            return master;
        }

        private bool CheckAuthAccess()
        {            
            string DeviceMAC = string.Empty;
            var _responseMAC = new GetMACAddress();
            DeviceMAC = CommonMethod.GetMacAddress().ToString();
            _responseMAC = GetMACAdress(DeviceMAC);

            if (_responseMAC.MACAddress == DeviceMAC)
            {

                return true;
            }
            else
            {
               // MessageBox.Show("Sorry !! your machine is not authorized for Channal Configration, Please contact to Administrtor");                
                return false;
            }           
        }


        private void SETFireWallEnable()
        {
            try
            {
                Process proc = new Process();
                string top = "netsh.exe";
                proc.StartInfo.Arguments = "Firewall set opmode enable";
                proc.StartInfo.FileName = top;
                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();
                proc.WaitForExit();
               // MessageBox.Show("Enable");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error");
            }
        }


        private void txtPmsCustCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                MessageBox.Show("Please Click to Fetch Property Id, Group Code Information  !!");
                MasterDataRequest request =  GetMasterDatafromDB(txtPmsCustCode.Text.ToString());
                txtPropertyId.Text = request.PropertyId;
                txtGroupCode.Text = request.GroupCode;

            }

            
        }

        void RefreshCache()
        {
            for (int i = 0; i < 1000; i++)
            {
                Thread.Sleep(10);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DHResponseModel dhRefreshCache = new DHResponseModel();
            string Url = ""; ;
            dhRefreshCache = SharkPost(Url, "",_dbConnectionType);

            using (FrmWait frm = new FrmWait(RefreshCache))
            {
                frm.ShowDialog(this);
            }



            //if (dhRefreshCache.MESSAGE == "Success")
            //{
            //    Thread.Sleep(10000);
            //    MessageBox.Show("Cache Refreshed.....");
            //}          



        }

        public static DataTable GetAppSettingUrlData()
        {
            DataTable sourceData = new DataTable();
            using (SqlConnection sqlcon = new SqlConnection(_dbConnection))
            {
                using (SqlCommand cmd = new SqlCommand("[dbo].[GetFxDistibutionAppSetting]", sqlcon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {


                        da.Fill(sourceData);
                    }
                }
            }

            return sourceData;
        }


        public static List<DataTable> RetrieveSourceData()
        {
            List<DataTable> sourceData = new List<DataTable>();

            DataTable dt0 = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            DataTable dt4 = new DataTable();
            DataTable dt5 = new DataTable();
            DataTable dt6 = new DataTable();
            DataTable dt7 = new DataTable();

            dt0 = FetchData.RetrieveSourceDataPropertyData(_filePath);
            dt1 = FetchData.RetrieveSourceDataAddon(_filePath);
            dt2 = FetchData.RetrieveSourceDataPickDrop(_filePath);
            dt3 = FetchData.RetrieveSourceDataBillingInstruction(_filePath);
            dt4 = FetchData.RetrieveSourceDataBusinessSource(_filePath);
            dt5 = FetchData.RetrieveSourceDataMarketSegment(_filePath);
            dt6 = FetchData.RetrieveSourceDataPaymentMode(_filePath);
            dt7 = FetchData.RetrieveSourceDataRevenueCode(_filePath);


            sourceData.Add(dt0);
            sourceData.Add(dt1);
            sourceData.Add(dt2);
            sourceData.Add(dt3);
            sourceData.Add(dt4);
            sourceData.Add(dt5);
            sourceData.Add(dt6);
            sourceData.Add(dt7);

            //string sourceConnString = string.Format(Excel03ConString, _filePath, "YES");
            //DataTable sourceData = new DataTable();
            //using (OleDbConnection conn = new OleDbConnection(sourceConnString))
            //{
            //    conn.Open();
            //    // Get the data from the source table as a SqlDataReader.
            //    OleDbCommand command = new OleDbCommand(
            //                        @"SELECT [ID],[Pmscustcode],[PropertyId],[IFSCCode],[Code],[Description],[Thirdpartycode],[ThirdpartycodeDesc] 
            //                         FROM [AddOn$]", conn);
            //    DataColumn dataColumn = new DataColumn();
            //    OleDbDataAdapter adapter = new OleDbDataAdapter(command);
            //    adapter.Fill(sourceData);
            //    conn.Close();
            //}          

            return sourceData;
        }

        public static void CopyData(List<DataTable> sourceData)
        {
            //GetValue<T>(sourceData);
            var _Output = string.Empty;
            string destConnString = _dbConnection;

            List<FXPMSAddOnMapping> Addonmapping = new List<FXPMSAddOnMapping>();
            Addonmapping = CommonMethod.ConvertToList<FXPMSAddOnMapping>(sourceData[1]);

            List<FXPMSPickupDropMapping> Pickdropmapping = new List<FXPMSPickupDropMapping>();
            Pickdropmapping = CommonMethod.ConvertToList<FXPMSPickupDropMapping>(sourceData[2]);

            List<FXPMSBillingInstructionMapping> BillignInsmapping = new List<FXPMSBillingInstructionMapping>();
            BillignInsmapping = CommonMethod.ConvertToList<FXPMSBillingInstructionMapping>(sourceData[3]);

            List<FXPMSBusinessSourceMapping> BusinessSourcemapping = new List<FXPMSBusinessSourceMapping>();
            BusinessSourcemapping = CommonMethod.ConvertToList<FXPMSBusinessSourceMapping>(sourceData[4]);

            List<FXPMSMarketSegmentMapping> MarketSegmentMapping = new List<FXPMSMarketSegmentMapping>();
            MarketSegmentMapping = CommonMethod.ConvertToList<FXPMSMarketSegmentMapping>(sourceData[5]);

            List<FXPMSPaymodeMapping> Paymodemapping = new List<FXPMSPaymodeMapping>();
            Paymodemapping = CommonMethod.ConvertToList<FXPMSPaymodeMapping>(sourceData[6]);

            List<FXPMSRevenueCodeMapping> RevenueMapping = new List<FXPMSRevenueCodeMapping>();
            RevenueMapping = CommonMethod.ConvertToList<FXPMSRevenueCodeMapping>(sourceData[7]);


            var addone = _ProcessAddOn(Addonmapping);
            var pickdrop = _ProcessPickupDrop(Pickdropmapping);
            var billingIns = _ProcessBillingInstruction(BillignInsmapping);
            var businesssource = _ProcessBusinessSource(BusinessSourcemapping);
            var marketsegment = _ProcessMarketSegment(MarketSegmentMapping);
            var paymode = _ProcessPayMode(Paymodemapping);
            var revenueCode = _ProcessRevenueCode(RevenueMapping);



            var _SerilizeAddon = JsonConvert.SerializeObject(addone);
            var _SerilizePickupDrop = JsonConvert.SerializeObject(pickdrop);
            var _SerilizeBilling = JsonConvert.SerializeObject(billingIns);
            var _SerilizeBusinessSource = JsonConvert.SerializeObject(businesssource);
            var _SerializeMarketSegment = JsonConvert.SerializeObject(marketsegment);
            var _SerializePaymode = JsonConvert.SerializeObject(paymode);
            var _SerializeRevenue = JsonConvert.SerializeObject(revenueCode);


            try
            {
                using (SqlConnection con = new SqlConnection(destConnString))
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[SP_Distribution_Parameters_Configurations]", con);
                   // SqlCommand cmd = new SqlCommand("[dbo].[SP_Distribution_Parameters_ConfigurationsTestQA]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 200000;
                    cmd.Parameters.Add(new SqlParameter("@FxDistAddOn", _SerilizeAddon));
                    cmd.Parameters.Add(new SqlParameter("@FxDistPickupDrop", _SerilizePickupDrop));
                    cmd.Parameters.Add(new SqlParameter("@FxDistPaymode", _SerializePaymode));
                    cmd.Parameters.Add(new SqlParameter("@FxDistBilling", _SerilizeBilling));
                    cmd.Parameters.Add(new SqlParameter("@FxDistBusinessSource", _SerilizeBusinessSource));
                    cmd.Parameters.Add(new SqlParameter("@FxDistMarketSegment", _SerializeMarketSegment));
                    cmd.Parameters.Add(new SqlParameter("@FxDistRevenueCode", _SerializeRevenue));

                    con.Open();
                    cmd.ExecuteReader();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());                
            }


        }

        private static List<FxDist_AddonMapping> _ProcessAddOn(List<FXPMSAddOnMapping> mapping)
        {
            List<FxDist_AddonMapping> _fxAddonList = new List<FxDist_AddonMapping>();


            foreach (var item in mapping)
            {
                FxDist_AddonMapping AddonData = new FxDist_AddonMapping();
                AddonData.Pmscustcode = Convert.ToInt64(_SET_PMSCustCode.ToString());//Convert.ToInt64(item.Pmscustcode);
                AddonData.PropertyId = Convert.ToInt64(_SET_PropertyID.ToString());
                AddonData.IFSCCode = item.IFSCCode.ToString();
                AddonData.Code = item.Code.ToString();
                AddonData.Description = item.Description.ToString();
                AddonData.Thirdpartycode = item.Thirdpartycode.ToString();
                AddonData.ThirdpartycodeDesc = item.ThirdpartycodeDesc.ToString();
                AddonData.ModifyDateTime = DateTime.Now;
                AddonData.UserId = "interface@idsnext.com";
                AddonData.IsDefault = item.IsDefault; 
                AddonData.IsActive = true;

                _fxAddonList.Add(AddonData);
            }

            return _fxAddonList;
        }

        private static List<FxDist_PickupDropMapping> _ProcessPickupDrop(List<FXPMSPickupDropMapping> mapping)
        {
            List<FxDist_PickupDropMapping> _fxPickupDropList = new List<FxDist_PickupDropMapping>();


            foreach (var item in mapping)
            {
                FxDist_PickupDropMapping PickDropData = new FxDist_PickupDropMapping();
                PickDropData.Pmscustcode = Convert.ToInt64(_SET_PMSCustCode.ToString()); //Convert.ToInt64(item.Pmscustcode);
                PickDropData.PropertyId = Convert.ToInt64(_SET_PropertyID.ToString());
                PickDropData.ParameterType = "DATAHUB";
                PickDropData.Code = "PCK";
                PickDropData.IFSCCode = item.IFSCCode.ToString();
                PickDropData.Pmscode = !string.IsNullOrEmpty(item.Code) ? item.Code : "";//item.Code.ToString();
                PickDropData.PmscodeDesc = !string.IsNullOrEmpty(item.Description) ? item.Description : "";//item.Description.ToString();
                PickDropData.Thirdpartycode = !string.IsNullOrEmpty(item.Thirdpartycode) ? item.Thirdpartycode : ""; //item.Thirdpartycode.ToString();
                PickDropData.ThirdpartycodeDesc = !string.IsNullOrEmpty(item.ThirdpartycodeDesc) ? item.ThirdpartycodeDesc : "";//item.ThirdpartycodeDesc.ToString();
                PickDropData.ModifyDateTime = DateTime.Now;
                PickDropData.UserId = "interface@idsnext.com";
                PickDropData.IsDefault = item.IsDefault;
                PickDropData.IsActive = true;

                _fxPickupDropList.Add(PickDropData);
            }

            return _fxPickupDropList;
        }

        private static List<FxDist_PaymodeMapping> _ProcessPayMode(List<FXPMSPaymodeMapping> mapping)
        {
            List<FxDist_PaymodeMapping> _fxPayModeList = new List<FxDist_PaymodeMapping>();


            foreach (var item in mapping)
            {
                FxDist_PaymodeMapping PayModeData = new FxDist_PaymodeMapping();
                PayModeData.Pmscustcode = Convert.ToInt64(_SET_PMSCustCode.ToString()); //Convert.ToInt64(item.Pmscustcode);
                PayModeData.PropertyId = Convert.ToInt64(_SET_PropertyID.ToString());
                PayModeData.ParameterType = "DATAHUB";
                PayModeData.Code = "PCK";
                PayModeData.IFSCCode = item.IFSCCode.ToString();
                PayModeData.Pmscode = !string.IsNullOrEmpty(item.Code) ? item.Code : "";//item.Code.ToString();
                PayModeData.PmscodeDesc = !string.IsNullOrEmpty(item.Description) ? item.Description : "";//item.Description.ToString();
                PayModeData.Thirdpartycode = !string.IsNullOrEmpty(item.Thirdpartycode) ? item.Thirdpartycode : ""; //item.Thirdpartycode.ToString();
                PayModeData.ThirdpartycodeDesc = !string.IsNullOrEmpty(item.ThirdpartycodeDesc) ? item.ThirdpartycodeDesc : "";//item.ThirdpartycodeDesc.ToString();
                PayModeData.ModifyDateTime = DateTime.Now;
                PayModeData.UserId = "interface@idsnext.com";
                PayModeData.IsDefault = item.IsDefault;
                PayModeData.IsActive = true;

                _fxPayModeList.Add(PayModeData);
            }

            return _fxPayModeList;
        }

        private static List<FxDist_BillingInstructionMapping> _ProcessBillingInstruction(List<FXPMSBillingInstructionMapping> mapping)
        {
            List<FxDist_BillingInstructionMapping> _fxBillingList = new List<FxDist_BillingInstructionMapping>();


            foreach (var item in mapping)
            {
                FxDist_BillingInstructionMapping BillingInsData = new FxDist_BillingInstructionMapping();
                BillingInsData.Pmscustcode = Convert.ToInt64(_SET_PMSCustCode.ToString()); //Convert.ToInt64(item.Pmscustcode);
                BillingInsData.PropertyId = Convert.ToInt64(_SET_PropertyID.ToString());
                BillingInsData.IFSCCode = item.IFSCCode.ToString();
                BillingInsData.Pmscode = !string.IsNullOrEmpty(item.Code) ? item.Code : "";//item.Code.ToString();
                BillingInsData.Description = !string.IsNullOrEmpty(item.Description) ? item.Description : "";//item.Description.ToString();
                BillingInsData.Thirdpartycode = !string.IsNullOrEmpty(item.Thirdpartycode) ? item.Thirdpartycode : ""; //item.Thirdpartycode.ToString();
                BillingInsData.ThirdpartycodeDesc = !string.IsNullOrEmpty(item.ThirdpartycodeDesc) ? item.ThirdpartycodeDesc : "";//item.ThirdpartycodeDesc.ToString();
                BillingInsData.ModifyDateTime = DateTime.Now;
                BillingInsData.UserId = "interface@idsnext.com";
                BillingInsData.IsDefault = item.IsDefault;
                BillingInsData.IsActive = true;

                _fxBillingList.Add(BillingInsData);
            }

            return _fxBillingList;
        }

        private static List<FxDist_RevenueCodeMapping> _ProcessRevenueCode(List<FXPMSRevenueCodeMapping> mapping)
        {
            List<FxDist_RevenueCodeMapping> _fxRevenueCodeList = new List<FxDist_RevenueCodeMapping>();


            foreach (var item in mapping)
            {
                FxDist_RevenueCodeMapping RevenueCodeData = new FxDist_RevenueCodeMapping();
                RevenueCodeData.Pmscustcode = Convert.ToInt64(_SET_PMSCustCode.ToString()); //Convert.ToInt64(item.Pmscustcode);
                RevenueCodeData.PropertyId = Convert.ToInt64(_SET_PropertyID.ToString());
                RevenueCodeData.IFSCCode = item.IFSCCode.ToString();
                RevenueCodeData.Code = !string.IsNullOrEmpty(item.Code) ? item.Code : "";//item.Code.ToString();
                RevenueCodeData.Description = !string.IsNullOrEmpty(item.Description) ? item.Description : "";//item.Description.ToString();
                RevenueCodeData.Thirdpartycode = !string.IsNullOrEmpty(item.Thirdpartycode) ? item.Thirdpartycode : ""; //item.Thirdpartycode.ToString();
                RevenueCodeData.ThirdpartycodeDesc = !string.IsNullOrEmpty(item.ThirdpartycodeDesc) ? item.ThirdpartycodeDesc : "";//item.ThirdpartycodeDesc.ToString();
                RevenueCodeData.ModifyDateTime = DateTime.Now;
                RevenueCodeData.UserId = "interface@idsnext.com";
                RevenueCodeData.IsDefault = item.IsDefault;
                RevenueCodeData.IsActive = true;

                _fxRevenueCodeList.Add(RevenueCodeData);
            }

            return _fxRevenueCodeList;
        }

        private static List<FxDist_BusinessSourceMapping> _ProcessBusinessSource(List<FXPMSBusinessSourceMapping> mapping)
        {
            List<FxDist_BusinessSourceMapping> _fxBusinessSourceList = new List<FxDist_BusinessSourceMapping>();


            foreach (var item in mapping)
            {
                FxDist_BusinessSourceMapping BusinessSourceData = new FxDist_BusinessSourceMapping();
                BusinessSourceData.Pmscustcode = Convert.ToInt64(_SET_PMSCustCode.ToString()); //Convert.ToInt64(item.Pmscustcode);
                BusinessSourceData.PropertyId = Convert.ToInt64(_SET_PropertyID.ToString());
                BusinessSourceData.ParameterType = "DATAHUB";
                BusinessSourceData.IFSCCode = item.IFSCCode.ToString();
                BusinessSourceData.Pmscode = !string.IsNullOrEmpty(item.Code) ? item.Code : "";//item.Code.ToString();
                BusinessSourceData.PmscodeDesc = !string.IsNullOrEmpty(item.Description) ? item.Description : "";//item.Description.ToString();
                BusinessSourceData.Thirdpartycode = !string.IsNullOrEmpty(item.Thirdpartycode) ? item.Thirdpartycode : ""; //item.Thirdpartycode.ToString();
                BusinessSourceData.ThirdpartycodeDesc = !string.IsNullOrEmpty(item.ThirdpartycodeDesc) ? item.ThirdpartycodeDesc : "";//item.ThirdpartycodeDesc.ToString();
                BusinessSourceData.ModifyDateTime = DateTime.Now;
                BusinessSourceData.UserId = "interface@idsnext.com";
                BusinessSourceData.IsDefault = item.IsDefault;
                //BusinessSourceData.
                BusinessSourceData.IsActive = true;

                _fxBusinessSourceList.Add(BusinessSourceData);
            }

            return _fxBusinessSourceList;
        }


        private static List<FxDist_MarketSegmentMapping> _ProcessMarketSegment(List<FXPMSMarketSegmentMapping> mapping)
        {
            List<FxDist_MarketSegmentMapping> _fxMarketSegmentList = new List<FxDist_MarketSegmentMapping>();


            foreach (var item in mapping)
            {
                FxDist_MarketSegmentMapping MarketSegmentData = new FxDist_MarketSegmentMapping();
                MarketSegmentData.Pmscustcode = Convert.ToInt64(_SET_PMSCustCode.ToString()); //Convert.ToInt64(item.Pmscustcode);
                MarketSegmentData.PropertyId = Convert.ToInt64(_SET_PropertyID.ToString());
                MarketSegmentData.ParameterType = "DATAHUB";
                MarketSegmentData.IFSCCode = item.IFSCCode.ToString();
                MarketSegmentData.Pmscode = !string.IsNullOrEmpty(item.Code) ? item.Code : "";//item.Code.ToString();
                MarketSegmentData.PmscodeDesc = !string.IsNullOrEmpty(item.Description) ? item.Description : "";//item.Description.ToString();
                MarketSegmentData.Thirdpartycode = !string.IsNullOrEmpty(item.Thirdpartycode) ? item.Thirdpartycode : ""; //item.Thirdpartycode.ToString();
                MarketSegmentData.ThirdpartycodeDesc = !string.IsNullOrEmpty(item.ThirdpartycodeDesc) ? item.ThirdpartycodeDesc : "";//item.ThirdpartycodeDesc.ToString();
                MarketSegmentData.ModifyDateTime = DateTime.Now;
                MarketSegmentData.UserId = "interface@idsnext.com";
                MarketSegmentData.IsDefault = item.IsDefault;
                //MarketSegmentData
                MarketSegmentData.IsActive = true;

                _fxMarketSegmentList.Add(MarketSegmentData);
            }

            return _fxMarketSegmentList;
        }


        private void ChannelInterfaceAPIMethod()
        {
            DataTable dt = GetAppSettingUrlData();
            List<URLAppSettingAppSetting> uRLAppSettingAppSettings = CommonMethod.ConvertToList<URLAppSettingAppSetting>(dt);

            List<FxDist_ChannelManagerApiDetails> fxDist_ChannelManagerApiDetailsList = new List<FxDist_ChannelManagerApiDetails>();

            List<FxDist_InterfaceApiDetails> _InterfaceApiDetailsList = new List<FxDist_InterfaceApiDetails>();


            if (chkDHReservation.Checked == true)
            {

                foreach (var item in uRLAppSettingAppSettings)
                {
                    FxDist_ChannelManagerApiDetails _channelData = new FxDist_ChannelManagerApiDetails();

                    _channelData.Pmscustcode = Convert.ToInt64(txtPmsCustCode.Text.ToString());
                    _channelData.PropertyId = Convert.ToInt64(txtPropertyId.Text.ToString());
                    _channelData.IFSCCode = item.InterfaceCode.ToString();
                    _channelData.ApiUsedFor = item.MessageType.ToString();
                    _channelData.ApiUserName = "";
                    _channelData.ApiPassword = "";
                    _channelData.APIAuthenticationKey = "";
                    _channelData.APIUrl = item.ChannelURL.ToString();
                    _channelData.ModifyDatetime = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));
                    _channelData.UserId = "interface@idsnext.com";
                    _channelData.IsActive = true;

                    fxDist_ChannelManagerApiDetailsList.Add(_channelData);
                }

            }

            List<MenageUrlMapping> _manageUrl = new List<MenageUrlMapping>();
            List<MenageUrlMapping> _manageUrlInputDH = new List<MenageUrlMapping>();
            List<MenageUrlMapping> _manageUrlOutDH = new List<MenageUrlMapping>();

            List<MenageUrlMapping> _manageUrlInputFXFD = new List<MenageUrlMapping>();
            List<MenageUrlMapping> _manageUrlOutFXFD = new List<MenageUrlMapping>();

            List<MenageUrlMapping> _manageUrlInputFXCRS = new List<MenageUrlMapping>();
            List<MenageUrlMapping> _manageUrlOutFXCRS = new List<MenageUrlMapping>();

            // GET DH
            _manageUrlInputDH = uRLAppSettingAppSettings.Where(x => x.InterfaceCode == "DATAHUB").Select(x => new MenageUrlMapping
            {
                ID = x.ID,
                MessageType = x.MessageType,
                ManageUrl = x.INURL,
                InterfaceCode = x.InterfaceCode,
                CommsType = "IN"

            }).ToList();

            _manageUrl.AddRange(_manageUrlInputDH);

            _manageUrlOutDH = uRLAppSettingAppSettings.Where(x => x.InterfaceCode == "DATAHUB").Select(x => new MenageUrlMapping
            {
                ID = x.ID,
                MessageType = x.MessageType,
                ManageUrl = x.OUTURL,
                InterfaceCode = x.InterfaceCode,
                CommsType = "OUT"
            }).ToList();

            _manageUrl.AddRange(_manageUrlOutDH);


            // GEt FAFD
            _manageUrlInputFXFD = uRLAppSettingAppSettings.Where(x => x.InterfaceCode == "FXFD").Select(x => new MenageUrlMapping
            {
                ID = x.ID,
                MessageType = x.MessageType,
                ManageUrl = x.INURL,
                InterfaceCode = x.InterfaceCode,
                CommsType = "IN"
            }).ToList();

            _manageUrl.AddRange(_manageUrlInputFXFD);

            _manageUrlOutFXFD = uRLAppSettingAppSettings.Where(x => x.InterfaceCode == "FXFD").Select(x => new MenageUrlMapping
            {
                ID = x.ID,
                MessageType = x.MessageType,
                ManageUrl = x.OUTURL,
                InterfaceCode = x.InterfaceCode,
                CommsType = "OUT"
            }).ToList();

            _manageUrl.AddRange(_manageUrlOutFXFD);

            //  FXCRS 
            _manageUrlInputFXCRS = uRLAppSettingAppSettings.Where(x => x.InterfaceCode == "FXCRS").Select(x => new MenageUrlMapping
            {
                ID = x.ID,
                MessageType = x.MessageType,
                ManageUrl = x.INURL,
                InterfaceCode = x.InterfaceCode,
                CommsType = "IN"
            }).ToList();

            _manageUrl.AddRange(_manageUrlInputFXCRS);

            _manageUrlOutFXCRS = uRLAppSettingAppSettings.Where(x => x.InterfaceCode == "FXCRS").Select(x => new MenageUrlMapping
            {
                ID = x.ID,
                MessageType = x.MessageType,
                ManageUrl = x.OUTURL,
                InterfaceCode = x.InterfaceCode,
                CommsType = "OUT"
            }).ToList();

            _manageUrl.AddRange(_manageUrlOutFXCRS);


            var c = _manageUrl;

            foreach (var item in _manageUrl)
            {
                FxDist_InterfaceApiDetails _interfaceData = new FxDist_InterfaceApiDetails();

                _interfaceData.Pmscustcode = Convert.ToInt64(txtPmsCustCode.Text.ToString());
                _interfaceData.PropertyId = Convert.ToInt64(txtPropertyId.Text.ToString());
                if (item.InterfaceCode == "DATAHUB")
                {
                    _interfaceData.SourceInterface = "DATAHUB";
                    _interfaceData.DestinationInterface = "FXCRS";
                }
                if (item.InterfaceCode == "FXCRS")
                {
                    _interfaceData.SourceInterface = "FXCRS";
                    _interfaceData.DestinationInterface = "DATAHUB";
                }                

                if (item.InterfaceCode == "FXFD")
                {
                    _interfaceData.SourceInterface = "FXFD";
                    _interfaceData.DestinationInterface = "FXCRS";
                }

                if (item.InterfaceCode == "FXFD" && item.MessageType == "Rate")
                {
                    _interfaceData.SourceInterface = "FXFD";
                    _interfaceData.DestinationInterface = "DATAHUB";
                }


                _interfaceData.ApiUsedFor = item.MessageType.ToString();
                _interfaceData.CommunicationType = item.CommsType.ToString();
                _interfaceData.IFSCCode = item.InterfaceCode.ToString();
                _interfaceData.ApiUserName = "";
                _interfaceData.ApiPassword = "";
                _interfaceData.APIAuthenticationKey = "";
                _interfaceData.APIUrl = item.ManageUrl.ToString();
                _interfaceData.ModifyDatetime = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));
                _interfaceData.UserId = "interface@idsnext.com";
                _interfaceData.IsActive = true;

                _InterfaceApiDetailsList.Add(_interfaceData);
            }

            var _channelSerializer = JsonConvert.SerializeObject(fxDist_ChannelManagerApiDetailsList);
            var _interfaceSerilizer = JsonConvert.SerializeObject(_InterfaceApiDetailsList);

            using (SqlConnection con = new SqlConnection(_dbConnection))
            {
                SqlCommand cmd = new SqlCommand("[dbo].[SP_Distribution_Channel_InterfaceConfiguration]", con);
                //SqlCommand cmd = new SqlCommand("[dbo].[SP_Distribution_Channel_InterfaceConfigurationTestQA]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 200000;
                cmd.Parameters.Add(new SqlParameter("@FxDistChannelApi", _channelSerializer));
                cmd.Parameters.Add(new SqlParameter("@FxDistInterfaceApi", _interfaceSerilizer));

                con.Open();
                cmd.ExecuteReader();
                con.Close();
            }

            Thread.Sleep(1000);

        }



        public static DHResponseModel SharkPost(string Url, object model,string typeConnection)
        {
            var _Output = new DHResponseModel();

            if (typeConnection == "QA")
            {
                Url = "https://qafxdistributioncachemanagement.azurewebsites.net/api/Cache/RefreshCacheData";
            }
            if (typeConnection == "Production")
            {
                Url = "https://fxdistcachemanagement.azurewebsites.net/api/Cache/RefreshCacheData";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Url);
                    var Records = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                    var response = client.PostAsync(Url, Records).Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        _Output.STATUS = true;
                        _Output.MESSAGE = response.StatusCode.ToString();
                    }
                    else
                    {
                        var responseContent = response.Content;
                        _Output.STATUS = true;
                        if (response.StatusCode.ToString() == "OK")
                            _Output.MESSAGE = "Success";

                        //_Output = _OutputConvertToString;
                    }
                }
            }
            catch (Exception ex)
            {
                _Output.STATUS = false;
                _Output.MESSAGE = ex.Message.ToString();
            }

            return _Output;

        }

        public class DHResponseModel
        {
            public bool STATUS { get; set; }
            public string MESSAGE { get; set; }
        }

        private void BrowseButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog od = new OpenFileDialog();
            od.Filter = "Excell|*.xls;*.xlsx;";
            DialogResult dr = od.ShowDialog();
            if (dr == DialogResult.Abort)
                return;
            if (dr == DialogResult.Cancel)
                return;
            txtBrowse.Text = od.FileName.ToString();
            _filePath = txtBrowse.Text;
        }

        private void UploadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtBrowse.Text == string.Empty)
                {
                    MessageBox.Show("Please enter a value to Excel File !!");
                    return;
                } 
                else
                {
                    List<DataTable> data = RetrieveSourceData();
                    DataRow[] dr = data[0].Select();
                    string _pmscustcode = string.Empty;
                    string _productCode = string.Empty;
                    foreach (DataRow row in dr)
                    {
                        _productCode = row["PropertyCode"].ToString();
                        _pmscustcode = row["CustomerCode"].ToString();
                        
                    }

                    //if (isValueExist("FxDist_ChannelManagerApiDetails", "Pmscustcode", _pmscustcode))
                    //{
                    //    MessageBox.Show("Customer Code "+ _pmscustcode + " Configured, Please update Excel Data & Try to Reinsert again !!");
                    //    return;
                    //}

                    //if (isValueExist("FxDist_InterfaceApiDetails", "Pmscustcode", _pmscustcode))
                    //{
                    //    MessageBox.Show("Customer Code " + _pmscustcode + " Configured, Please update Excel Data & Try to Reinsert again !!");
                    //    return;
                    //}


                    MasterDataRequest request = GetMasterDatafromDB(_pmscustcode);
                    txtPmsCustCode.Text = _pmscustcode;
                    txtPropertyId.Text = request.PropertyId;
                    txtGroupCode.Text = request.GroupCode;
                    txtPropertyCode.Text = _productCode;

                    _SET_PMSCustCode = _pmscustcode;
                    _SET_PropertyID = request.PropertyId;


                    CopyData(data);
                    ChannelInterfaceAPIMethod();


                    MessageBox.Show("File imported into sql server.");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString());
            }
        }

        // Validation  

        public static bool isValueExist(string tableName,string fieldName,string columnValue)
        {
            bool status = true;
            string constr = _dbConnection;
            using (SqlConnection conn = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT COUNT(*) from "+ tableName+ " where "+fieldName+" = "+ columnValue + "" , conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("columnValue", columnValue);
                    conn.Open();
                    status = Convert.ToBoolean(cmd.ExecuteScalar());
                    conn.Close();
                }
            }

            return status;
        }

    }
}
