﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributionConfiguration.BusinessObject.Model
{
    public class URLAppSettingAppSetting  
    {  
        public int ID { get; set; }
        public string MessageType { get; set; }
        public string INURL { get; set; }
        public string OUTURL { get; set; }
        public string ChannelURL { get; set; }
        public string InterfaceCode { get; set; }
    }

    public class MenageUrlMapping
    { 
        public int ID { get; set; }
        public string MessageType { get; set; }
        public string ManageUrl { get; set; }
        public string InterfaceCode { get; set; }
        public string CommsType { get; set; }
    }
}
