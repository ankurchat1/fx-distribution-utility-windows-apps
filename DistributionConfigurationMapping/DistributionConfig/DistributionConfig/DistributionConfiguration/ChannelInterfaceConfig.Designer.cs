﻿namespace DistributionConfiguration
{
    partial class ChannelInterfaceConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtPropertyId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPMSCustCode = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.chkDHGuest = new System.Windows.Forms.CheckBox();
            this.chkDHCompany = new System.Windows.Forms.CheckBox();
            this.chkDHHotelPosition = new System.Windows.Forms.CheckBox();
            this.chkDHReservation = new System.Windows.Forms.CheckBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.chkCRSGroupBlock = new System.Windows.Forms.CheckBox();
            this.chkCRSRateRestriction = new System.Windows.Forms.CheckBox();
            this.chkCRSRate = new System.Windows.Forms.CheckBox();
            this.chkCRSGuest = new System.Windows.Forms.CheckBox();
            this.chkCRSCompany = new System.Windows.Forms.CheckBox();
            this.chkCRSReservation = new System.Windows.Forms.CheckBox();
            this.btncancel = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(25, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 15);
            this.label1.TabIndex = 8;
            this.label1.Text = "Property ID";
            // 
            // txtPropertyId
            // 
            this.txtPropertyId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPropertyId.Location = new System.Drawing.Point(210, 56);
            this.txtPropertyId.MaxLength = 6;
            this.txtPropertyId.Name = "txtPropertyId";
            this.txtPropertyId.ReadOnly = true;
            this.txtPropertyId.Size = new System.Drawing.Size(271, 21);
            this.txtPropertyId.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(25, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "PMS Code";
            // 
            // txtPMSCustCode
            // 
            this.txtPMSCustCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPMSCustCode.Location = new System.Drawing.Point(210, 19);
            this.txtPMSCustCode.MaxLength = 6;
            this.txtPMSCustCode.Name = "txtPMSCustCode";
            this.txtPMSCustCode.ReadOnly = true;
            this.txtPMSCustCode.Size = new System.Drawing.Size(271, 21);
            this.txtPMSCustCode.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tabControl1);
            this.groupBox1.Location = new System.Drawing.Point(12, 100);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(469, 362);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "MessageType";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(6, 19);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(457, 308);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.chkDHGuest);
            this.tabPage1.Controls.Add(this.chkDHCompany);
            this.tabPage1.Controls.Add(this.chkDHHotelPosition);
            this.tabPage1.Controls.Add(this.chkDHReservation);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(449, 282);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "DATAHUB";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // chkDHGuest
            // 
            this.chkDHGuest.AutoSize = true;
            this.chkDHGuest.Location = new System.Drawing.Point(20, 129);
            this.chkDHGuest.Name = "chkDHGuest";
            this.chkDHGuest.Size = new System.Drawing.Size(54, 17);
            this.chkDHGuest.TabIndex = 3;
            this.chkDHGuest.Text = "Guest";
            this.chkDHGuest.UseVisualStyleBackColor = true;
            // 
            // chkDHCompany
            // 
            this.chkDHCompany.AutoSize = true;
            this.chkDHCompany.Location = new System.Drawing.Point(20, 93);
            this.chkDHCompany.Name = "chkDHCompany";
            this.chkDHCompany.Size = new System.Drawing.Size(70, 17);
            this.chkDHCompany.TabIndex = 2;
            this.chkDHCompany.Text = "Company";
            this.chkDHCompany.UseVisualStyleBackColor = true;
            // 
            // chkDHHotelPosition
            // 
            this.chkDHHotelPosition.AutoSize = true;
            this.chkDHHotelPosition.Location = new System.Drawing.Point(20, 58);
            this.chkDHHotelPosition.Name = "chkDHHotelPosition";
            this.chkDHHotelPosition.Size = new System.Drawing.Size(91, 17);
            this.chkDHHotelPosition.TabIndex = 1;
            this.chkDHHotelPosition.Text = "Hotel Position";
            this.chkDHHotelPosition.UseVisualStyleBackColor = true;
            // 
            // chkDHReservation
            // 
            this.chkDHReservation.AutoSize = true;
            this.chkDHReservation.Location = new System.Drawing.Point(20, 21);
            this.chkDHReservation.Name = "chkDHReservation";
            this.chkDHReservation.Size = new System.Drawing.Size(83, 17);
            this.chkDHReservation.TabIndex = 0;
            this.chkDHReservation.Text = "Reservation";
            this.chkDHReservation.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.chkCRSGroupBlock);
            this.tabPage2.Controls.Add(this.chkCRSRateRestriction);
            this.tabPage2.Controls.Add(this.chkCRSRate);
            this.tabPage2.Controls.Add(this.chkCRSGuest);
            this.tabPage2.Controls.Add(this.chkCRSCompany);
            this.tabPage2.Controls.Add(this.chkCRSReservation);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(449, 282);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "FX-CRS";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // chkCRSGroupBlock
            // 
            this.chkCRSGroupBlock.AutoSize = true;
            this.chkCRSGroupBlock.Location = new System.Drawing.Point(6, 216);
            this.chkCRSGroupBlock.Name = "chkCRSGroupBlock";
            this.chkCRSGroupBlock.Size = new System.Drawing.Size(85, 17);
            this.chkCRSGroupBlock.TabIndex = 13;
            this.chkCRSGroupBlock.Text = "Group Block";
            this.chkCRSGroupBlock.UseVisualStyleBackColor = true;
            
            // 
            // chkCRSRateRestriction
            // 
            this.chkCRSRateRestriction.AutoSize = true;
            this.chkCRSRateRestriction.Location = new System.Drawing.Point(18, 177);
            this.chkCRSRateRestriction.Name = "chkCRSRateRestriction";
            this.chkCRSRateRestriction.Size = new System.Drawing.Size(102, 17);
            this.chkCRSRateRestriction.TabIndex = 12;
            this.chkCRSRateRestriction.Text = "Rate Restriction";
            this.chkCRSRateRestriction.UseVisualStyleBackColor = true;
            // 
            // chkCRSRate
            // 
            this.chkCRSRate.AutoSize = true;
            this.chkCRSRate.Location = new System.Drawing.Point(18, 137);
            this.chkCRSRate.Name = "chkCRSRate";
            this.chkCRSRate.Size = new System.Drawing.Size(49, 17);
            this.chkCRSRate.TabIndex = 11;
            this.chkCRSRate.Text = "Rate";
            this.chkCRSRate.UseVisualStyleBackColor = true;
            // 
            // chkCRSGuest
            // 
            this.chkCRSGuest.AutoSize = true;
            this.chkCRSGuest.Location = new System.Drawing.Point(18, 96);
            this.chkCRSGuest.Name = "chkCRSGuest";
            this.chkCRSGuest.Size = new System.Drawing.Size(54, 17);
            this.chkCRSGuest.TabIndex = 10;
            this.chkCRSGuest.Text = "Guest";
            this.chkCRSGuest.UseVisualStyleBackColor = true;
            // 
            // chkCRSCompany
            // 
            this.chkCRSCompany.AutoSize = true;
            this.chkCRSCompany.Location = new System.Drawing.Point(18, 56);
            this.chkCRSCompany.Name = "chkCRSCompany";
            this.chkCRSCompany.Size = new System.Drawing.Size(70, 17);
            this.chkCRSCompany.TabIndex = 9;
            this.chkCRSCompany.Text = "Company";
            this.chkCRSCompany.UseVisualStyleBackColor = true;
            // 
            // chkCRSReservation
            // 
            this.chkCRSReservation.AutoSize = true;
            this.chkCRSReservation.Location = new System.Drawing.Point(18, 19);
            this.chkCRSReservation.Name = "chkCRSReservation";
            this.chkCRSReservation.Size = new System.Drawing.Size(83, 17);
            this.chkCRSReservation.TabIndex = 7;
            this.chkCRSReservation.Text = "Reservation";
            this.chkCRSReservation.UseVisualStyleBackColor = true;
            // 
            // btncancel
            // 
            this.btncancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncancel.Location = new System.Drawing.Point(342, 468);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(151, 23);
            this.btncancel.TabIndex = 13;
            this.btncancel.Text = "Cancel";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnect.Location = new System.Drawing.Point(167, 468);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(151, 23);
            this.btnConnect.TabIndex = 12;
            this.btnConnect.Text = "Save";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // ChannelInterfaceConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 503);
            this.Controls.Add(this.btncancel);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPropertyId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPMSCustCode);
            this.Name = "ChannelInterfaceConfig";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ChannelInterfaceConfig";
            this.Load += new System.EventHandler(this.ChannelInterfaceConfig_Load);
            this.groupBox1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPropertyId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPMSCustCode;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.CheckBox chkDHReservation;
        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.CheckBox chkDHCompany;
        private System.Windows.Forms.CheckBox chkDHHotelPosition;
        private System.Windows.Forms.CheckBox chkDHGuest;
        private System.Windows.Forms.CheckBox chkCRSGroupBlock;
        private System.Windows.Forms.CheckBox chkCRSRateRestriction;
        private System.Windows.Forms.CheckBox chkCRSRate;
        private System.Windows.Forms.CheckBox chkCRSGuest;
        private System.Windows.Forms.CheckBox chkCRSCompany;
        private System.Windows.Forms.CheckBox chkCRSReservation;
    }
}