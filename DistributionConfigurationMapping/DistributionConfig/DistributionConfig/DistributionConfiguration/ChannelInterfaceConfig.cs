﻿using DistributionConfiguration.BusinessObject;
using DistributionConfiguration.BusinessObject.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DistributionConfiguration
{
    public partial class ChannelInterfaceConfig : Form
    {
        public static string _dbConnection = string.Empty;        
        public static string FullPathDB = string.Empty;
        private static SqlConnection con;

        public static string _SET_PMSCustCode = "";
        public static string _SET_PropertyID = "";
        public static string _SET_GROUPCode = "";



        public ChannelInterfaceConfig()
        {
            InitializeComponent();
        }
              

        public ChannelInterfaceConfig(string _connecttion,string _pmsCustCode,string _propertyId,string grpCode)
        {
            _dbConnection = _connecttion;
            _SET_PMSCustCode = _pmsCustCode;
            _SET_PropertyID = _propertyId;
            _SET_GROUPCode = grpCode;

            InitializeComponent();
        }        

        private void btncancel_Click(object sender, EventArgs e)
        {
            FullPathDB = _dbConnection;

            _SET_PMSCustCode = txtPMSCustCode.Text.ToString();
            _SET_PropertyID = txtPropertyId.Text.ToString();
            

            FrmDistributionConfigMain obj1 = new FrmDistributionConfigMain(FullPathDB, _SET_PMSCustCode, _SET_PropertyID,_SET_GROUPCode);
            obj1.Show();
            this.Close();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPMSCustCode.Text == string.Empty)
                {
                    MessageBox.Show("Please enter PMS Customer Code !!");
                    return;
                }
                else if (txtPropertyId.Text == string.Empty)
                {
                    MessageBox.Show("Please enter Property ID !!");
                    return;
                }
                //else if (txtPropertyCode.Text == string.Empty)
                //{
                //    MessageBox.Show("Please enter the Property Code !!");
                //    return;
                //}
                else
                {
                    DataTable dt = GetAppSettingUrlData();
                    List<URLAppSettingAppSetting> uRLAppSettingAppSettings = CommonMethod.ConvertToList<URLAppSettingAppSetting>(dt);

                    List<FxDist_ChannelManagerApiDetails> fxDist_ChannelManagerApiDetailsList = new List<FxDist_ChannelManagerApiDetails>();
                    
                    List<FxDist_InterfaceApiDetails> _InterfaceApiDetailsList = new List<FxDist_InterfaceApiDetails>();                    
                    

                    if (chkDHReservation.Checked == true)
                    {                       

                        foreach (var item in uRLAppSettingAppSettings)
                        {
                            FxDist_ChannelManagerApiDetails _channelData = new FxDist_ChannelManagerApiDetails();

                            _channelData.Pmscustcode = Convert.ToInt64(txtPMSCustCode.Text.ToString());
                            _channelData.PropertyId = Convert.ToInt64(txtPropertyId.Text.ToString());
                            _channelData.IFSCCode = item.InterfaceCode.ToString();
                            _channelData.ApiUsedFor = item.MessageType.ToString();
                            _channelData.ApiUserName = "";
                            _channelData.ApiPassword = "";
                            _channelData.APIAuthenticationKey = "";
                            _channelData.APIUrl = item.ChannelURL.ToString();
                            _channelData.ModifyDatetime = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));
                            _channelData.UserId = "interface@idsnext.com";
                            _channelData.IsActive = true;

                            fxDist_ChannelManagerApiDetailsList.Add(_channelData);
                        }                       

                    }

                    List<MenageUrlMapping> _manageUrl = new List<MenageUrlMapping>();
                    List<MenageUrlMapping> _manageUrlInputDH = new List<MenageUrlMapping>();
                    List<MenageUrlMapping> _manageUrlOutDH = new List<MenageUrlMapping>();

                    List<MenageUrlMapping> _manageUrlInputFXFD = new List<MenageUrlMapping>();
                    List<MenageUrlMapping> _manageUrlOutFXFD = new List<MenageUrlMapping>();

                    List<MenageUrlMapping> _manageUrlInputFXCRS = new List<MenageUrlMapping>();
                    List<MenageUrlMapping> _manageUrlOutFXCRS = new List<MenageUrlMapping>();

                    // GET DH
                    _manageUrlInputDH = uRLAppSettingAppSettings.Where(x => x.InterfaceCode == "DATAHUB").Select(x => new MenageUrlMapping
                        {
                            ID = x.ID,
                            MessageType = x.MessageType,
                            ManageUrl = x.INURL,
                            InterfaceCode = x.InterfaceCode,
                            CommsType = "IN"
                            
                        }).ToList();

                    _manageUrl.AddRange(_manageUrlInputDH);

                    _manageUrlOutDH = uRLAppSettingAppSettings.Where(x => x.InterfaceCode == "DATAHUB").Select(x => new MenageUrlMapping
                        {
                            ID = x.ID,
                            MessageType = x.MessageType,
                            ManageUrl = x.OUTURL,
                            InterfaceCode = x.InterfaceCode,
                            CommsType = "OUT"
                    }).ToList();

                    _manageUrl.AddRange(_manageUrlOutDH);


                    // GEt FAFD
                    _manageUrlInputFXFD = uRLAppSettingAppSettings.Where(x => x.InterfaceCode == "FXFD").Select(x => new MenageUrlMapping
                    {
                        ID = x.ID,
                        MessageType = x.MessageType,
                        ManageUrl = x.INURL,
                        InterfaceCode = x.InterfaceCode,
                        CommsType = "IN"
                    }).ToList();

                    _manageUrl.AddRange(_manageUrlInputFXFD);

                    _manageUrlOutFXFD = uRLAppSettingAppSettings.Where(x => x.InterfaceCode == "FXFD").Select(x => new MenageUrlMapping
                    {
                        ID = x.ID,
                        MessageType = x.MessageType,
                        ManageUrl = x.OUTURL,
                        InterfaceCode = x.InterfaceCode,
                        CommsType = "OUT"
                    }).ToList();

                    _manageUrl.AddRange(_manageUrlOutFXFD);

                    //  FXCRS 
                    _manageUrlInputFXCRS = uRLAppSettingAppSettings.Where(x => x.InterfaceCode == "FXCRS").Select(x => new MenageUrlMapping
                    {
                        ID = x.ID,
                        MessageType = x.MessageType,
                        ManageUrl = x.INURL,
                        InterfaceCode = x.InterfaceCode,
                        CommsType = "IN"
                    }).ToList();

                    _manageUrl.AddRange(_manageUrlInputFXCRS);

                    _manageUrlOutFXCRS = uRLAppSettingAppSettings.Where(x => x.InterfaceCode == "FXCRS").Select(x => new MenageUrlMapping
                    {
                        ID = x.ID,
                        MessageType = x.MessageType,
                        ManageUrl = x.OUTURL,
                        InterfaceCode = x.InterfaceCode,
                        CommsType = "OUT"
                    }).ToList();

                    _manageUrl.AddRange(_manageUrlOutFXCRS);


                    var c = _manageUrl;

                    foreach (var item in _manageUrl)
                    {
                        FxDist_InterfaceApiDetails _interfaceData = new FxDist_InterfaceApiDetails();

                        _interfaceData.Pmscustcode = Convert.ToInt64(txtPMSCustCode.Text.ToString());
                        _interfaceData.PropertyId = Convert.ToInt64(txtPropertyId.Text.ToString());
                        if (item.InterfaceCode == "DATAHUB")
                        {
                            _interfaceData.SourceInterface = "DATAHUB";
                            _interfaceData.DestinationInterface = "FXCRS";
                        }
                        if (item.InterfaceCode == "FXCRS")
                        {
                            _interfaceData.SourceInterface = "FXCRS";
                            _interfaceData.DestinationInterface = "DATAHUB";
                        }

                        if (item.InterfaceCode == "FXFD" && item.MessageType == "Rate")
                        {
                            _interfaceData.SourceInterface = "FXFD";
                            _interfaceData.DestinationInterface = "DATAHUB";
                        }

                        if (item.InterfaceCode == "FXFD")
                        {
                            _interfaceData.SourceInterface = "FXFD";
                            _interfaceData.DestinationInterface = "FXCRS";
                        }

                        
                        _interfaceData.ApiUsedFor = item.MessageType.ToString();
                        _interfaceData.CommunicationType = item.CommsType.ToString();
                        _interfaceData.IFSCCode = item.InterfaceCode.ToString();                        
                        _interfaceData.ApiUserName = "";
                        _interfaceData.ApiPassword = "";
                        _interfaceData.APIAuthenticationKey = "";
                        _interfaceData.APIUrl = item.ManageUrl.ToString();
                        _interfaceData.ModifyDatetime = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));
                        _interfaceData.UserId = "interface@idsnext.com";
                        _interfaceData.IsActive = true;

                        _InterfaceApiDetailsList.Add(_interfaceData);
                    }

                    var _channelSerializer = JsonConvert.SerializeObject(fxDist_ChannelManagerApiDetailsList); 
                    var _interfaceSerilizer = JsonConvert.SerializeObject(_InterfaceApiDetailsList); 

                    using (SqlConnection con = new SqlConnection(_dbConnection))
                    {
                        SqlCommand cmd = new SqlCommand("[dbo].[SP_Distribution_Channel_InterfaceConfiguration]", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 200000;
                        cmd.Parameters.Add(new SqlParameter("@FxDistChannelApi", _channelSerializer));
                        cmd.Parameters.Add(new SqlParameter("@FxDistInterfaceApi", _interfaceSerilizer));                        

                        con.Open();
                        cmd.ExecuteReader();
                        con.Close();
                    }

                    Thread.Sleep(1000);
                }
                MessageBox.Show("Distribution Channel & Interface API Url Configured...");
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }
        }


        public static DataTable GetAppSettingUrlData()
        {
            DataTable sourceData = new DataTable();
            using (SqlConnection sqlcon = new SqlConnection(_dbConnection))
            {
                using (SqlCommand cmd = new SqlCommand("[dbo].[GetFxDistibutionAppSetting]", sqlcon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        

                        da.Fill(sourceData);
                    }
                }            
            }

            return sourceData;
        }

        private void ChannelInterfaceConfig_Load(object sender, EventArgs e)
        {
            LoadUserControl();
        }

        private void LoadUserControl()
        {
            chkDHReservation.Checked = true;
            chkDHHotelPosition.Checked = true;
            chkCRSReservation.Checked = true;
            

            txtPMSCustCode.Text = FrmDistributionConfigMain._SET_PMSCustCode.ToString();
            txtPropertyId.Text = FrmDistributionConfigMain._SET_PropertyID.ToString();

        }

        

        //private void txtPMSCustCode_TextChanged(object sender, EventArgs e)
        //{
        //    if (System.Text.RegularExpressions.Regex.IsMatch(txtPMSCustCode.Text, "[^0-9]"))
        //    {
        //        MessageBox.Show("Please enter only numbers.");
        //        txtPMSCustCode.Text = txtPMSCustCode.Text.Remove(txtPMSCustCode.Text.Length - 1);
        //    }
        //}

        //private void txtPropertyId_TextChanged(object sender, EventArgs e)
        //{
        //    if (System.Text.RegularExpressions.Regex.IsMatch(txtPropertyId.Text, "[^0-9]"))
        //    {
        //        MessageBox.Show("Please enter only numbers.");
        //        txtPropertyId.Text = txtPropertyId.Text.Remove(txtPropertyId.Text.Length - 1);
        //    }
        //}
    }
}
