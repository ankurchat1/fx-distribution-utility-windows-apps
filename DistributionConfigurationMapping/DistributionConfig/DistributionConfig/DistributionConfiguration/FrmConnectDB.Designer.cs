﻿namespace DistributionConfiguration
{
    partial class FrmConnectDB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConnectQA = new System.Windows.Forms.Button();
            this.btnConnectProd = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnConnectQA
            // 
            this.btnConnectQA.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnectQA.Location = new System.Drawing.Point(12, 38);
            this.btnConnectQA.Name = "btnConnectQA";
            this.btnConnectQA.Size = new System.Drawing.Size(223, 46);
            this.btnConnectQA.TabIndex = 0;
            this.btnConnectQA.Text = "Connect (QA)";
            this.btnConnectQA.UseVisualStyleBackColor = true;
            this.btnConnectQA.Click += new System.EventHandler(this.btnConnectQA_Click);
            // 
            // btnConnectProd
            // 
            this.btnConnectProd.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnectProd.Location = new System.Drawing.Point(241, 38);
            this.btnConnectProd.Name = "btnConnectProd";
            this.btnConnectProd.Size = new System.Drawing.Size(223, 46);
            this.btnConnectProd.TabIndex = 1;
            this.btnConnectProd.Text = "Connect (Production)";
            this.btnConnectProd.UseVisualStyleBackColor = true;
            this.btnConnectProd.Click += new System.EventHandler(this.btnConnectProd_Click);
            // 
            // FrmConnectDB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(490, 131);
            this.Controls.Add(this.btnConnectProd);
            this.Controls.Add(this.btnConnectQA);
            this.Name = "FrmConnectDB";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Make Connection";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnConnectQA;
        private System.Windows.Forms.Button btnConnectProd;
    }
}