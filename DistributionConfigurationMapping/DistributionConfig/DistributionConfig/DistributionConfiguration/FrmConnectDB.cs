﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DistributionConfiguration
{
    public partial class FrmConnectDB : Form
    {
        public FrmConnectDB()
        {
            InitializeComponent();
        }

        private string FullPathDB = string.Empty;
        private string _databaseNameQA = "Server=skyreslivedbserver.database.windows.net;Database=FXCRS_TESTDB;User ID=skyresadmin;Password=Sky@Res@123;Trusted_Connection=False";
        private string _databaseNameProduction = "Server=fxoneadmin.database.windows.net;Database=FX_Sabre_Interface_Log;User ID=fxoneadmin;Password=FXOneAdm@123;Trusted_Connection=False";

        private void btnConnectQA_Click(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show("Server Connected !! ");


                FullPathDB = _databaseNameQA;
                FrmDistributionConfigMain obj1 = new FrmDistributionConfigMain(FullPathDB);
                obj1.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void btnConnectProd_Click(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show("Server Connected !! ");


                FullPathDB = _databaseNameProduction;
                FrmDistributionConfigMain obj1 = new FrmDistributionConfigMain(FullPathDB);
                obj1.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
           
        }
    }
}
