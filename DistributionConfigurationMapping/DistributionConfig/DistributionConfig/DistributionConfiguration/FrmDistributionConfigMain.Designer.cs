﻿namespace DistributionConfiguration
{
    partial class FrmDistributionConfigMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnParameterConfig = new System.Windows.Forms.Button();
            this.btnInterfaceConfig = new System.Windows.Forms.Button();
            this.txtPmsCustCode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPropertyId = new System.Windows.Forms.Label();
            this.txtPropertyId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPropertyCode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtGroupCode = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.chkIsEnabled = new System.Windows.Forms.CheckBox();
            this.cmbSavePriority = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnChannalSave = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.chkIsFOMEnaled = new System.Windows.Forms.CheckBox();
            this.chkIsCRSEnabled = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnParameterConfig
            // 
            this.btnParameterConfig.Font = new System.Drawing.Font("Sitka Small", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnParameterConfig.Location = new System.Drawing.Point(12, 115);
            this.btnParameterConfig.Name = "btnParameterConfig";
            this.btnParameterConfig.Size = new System.Drawing.Size(155, 86);
            this.btnParameterConfig.TabIndex = 19;
            this.btnParameterConfig.Text = "Parameters Configuration";
            this.btnParameterConfig.UseVisualStyleBackColor = true;
            this.btnParameterConfig.Click += new System.EventHandler(this.btnParameterConfig_Click);
            // 
            // btnInterfaceConfig
            // 
            this.btnInterfaceConfig.Font = new System.Drawing.Font("Sitka Small", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInterfaceConfig.Location = new System.Drawing.Point(12, 12);
            this.btnInterfaceConfig.Name = "btnInterfaceConfig";
            this.btnInterfaceConfig.Size = new System.Drawing.Size(155, 86);
            this.btnInterfaceConfig.TabIndex = 22;
            this.btnInterfaceConfig.Text = "Channel Interface Configuration";
            this.btnInterfaceConfig.UseVisualStyleBackColor = true;
            this.btnInterfaceConfig.Click += new System.EventHandler(this.btnInterfaceConfig_Click);
            // 
            // txtPmsCustCode
            // 
            this.txtPmsCustCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPmsCustCode.Location = new System.Drawing.Point(427, 23);
            this.txtPmsCustCode.MaxLength = 8;
            this.txtPmsCustCode.Name = "txtPmsCustCode";
            this.txtPmsCustCode.Size = new System.Drawing.Size(330, 22);
            this.txtPmsCustCode.TabIndex = 24;
            this.txtPmsCustCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPmsCustCode_KeyDown);
            this.txtPmsCustCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPmsCustCode_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(206, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 16);
            this.label1.TabIndex = 25;
            this.label1.Text = "Enter PMS Customer Code :";
            // 
            // lblPropertyId
            // 
            this.lblPropertyId.AutoSize = true;
            this.lblPropertyId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPropertyId.Location = new System.Drawing.Point(206, 69);
            this.lblPropertyId.Name = "lblPropertyId";
            this.lblPropertyId.Size = new System.Drawing.Size(115, 16);
            this.lblPropertyId.TabIndex = 27;
            this.lblPropertyId.Text = "Enter Property ID :";
            // 
            // txtPropertyId
            // 
            this.txtPropertyId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPropertyId.Location = new System.Drawing.Point(427, 66);
            this.txtPropertyId.MaxLength = 8;
            this.txtPropertyId.Name = "txtPropertyId";
            this.txtPropertyId.ReadOnly = true;
            this.txtPropertyId.Size = new System.Drawing.Size(330, 22);
            this.txtPropertyId.TabIndex = 26;
            this.txtPropertyId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPropertyId_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(206, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 16);
            this.label2.TabIndex = 29;
            this.label2.Text = "Enter Property Code :";
            // 
            // txtPropertyCode
            // 
            this.txtPropertyCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPropertyCode.Location = new System.Drawing.Point(427, 112);
            this.txtPropertyCode.MaxLength = 8;
            this.txtPropertyCode.Name = "txtPropertyCode";
            this.txtPropertyCode.Size = new System.Drawing.Size(330, 22);
            this.txtPropertyCode.TabIndex = 28;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(206, 165);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 16);
            this.label3.TabIndex = 31;
            this.label3.Text = "Enter Group Code :";
            // 
            // txtGroupCode
            // 
            this.txtGroupCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGroupCode.Location = new System.Drawing.Point(427, 162);
            this.txtGroupCode.MaxLength = 8;
            this.txtGroupCode.Name = "txtGroupCode";
            this.txtGroupCode.ReadOnly = true;
            this.txtGroupCode.Size = new System.Drawing.Size(330, 22);
            this.txtGroupCode.TabIndex = 30;
            this.txtGroupCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGroupCode_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(206, 228);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(124, 16);
            this.label5.TabIndex = 34;
            this.label5.Text = "Enter Save Priority :";
            // 
            // chkIsEnabled
            // 
            this.chkIsEnabled.AutoSize = true;
            this.chkIsEnabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIsEnabled.Location = new System.Drawing.Point(209, 291);
            this.chkIsEnabled.Name = "chkIsEnabled";
            this.chkIsEnabled.Size = new System.Drawing.Size(237, 20);
            this.chkIsEnabled.TabIndex = 46;
            this.chkIsEnabled.Text = "Is Enabled  (Inventory Send To DH)";
            this.chkIsEnabled.UseVisualStyleBackColor = true;
            // 
            // cmbSavePriority
            // 
            this.cmbSavePriority.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSavePriority.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSavePriority.FormattingEnabled = true;
            this.cmbSavePriority.Items.AddRange(new object[] {
            "SELECT",
            "FXCRS",
            "FOM"});
            this.cmbSavePriority.Location = new System.Drawing.Point(427, 221);
            this.cmbSavePriority.Name = "cmbSavePriority";
            this.cmbSavePriority.Size = new System.Drawing.Size(183, 23);
            this.cmbSavePriority.TabIndex = 55;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(673, 369);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(101, 35);
            this.btnCancel.TabIndex = 57;
            this.btnCancel.Text = "Exit";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnChannalSave
            // 
            this.btnChannalSave.Location = new System.Drawing.Point(554, 369);
            this.btnChannalSave.Name = "btnChannalSave";
            this.btnChannalSave.Size = new System.Drawing.Size(101, 35);
            this.btnChannalSave.TabIndex = 56;
            this.btnChannalSave.Text = "Save";
            this.btnChannalSave.UseVisualStyleBackColor = true;
            this.btnChannalSave.Click += new System.EventHandler(this.btnChannalSave_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 221);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(155, 81);
            this.button1.TabIndex = 58;
            this.button1.Text = "Refresh Chache";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // chkIsFOMEnaled
            // 
            this.chkIsFOMEnaled.AutoSize = true;
            this.chkIsFOMEnaled.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIsFOMEnaled.Location = new System.Drawing.Point(633, 250);
            this.chkIsFOMEnaled.Name = "chkIsFOMEnaled";
            this.chkIsFOMEnaled.Size = new System.Drawing.Size(126, 20);
            this.chkIsFOMEnaled.TabIndex = 60;
            this.chkIsFOMEnaled.Text = "Is Enabled  FOM";
            this.chkIsFOMEnaled.UseVisualStyleBackColor = true;
            // 
            // chkIsCRSEnabled
            // 
            this.chkIsCRSEnabled.AutoSize = true;
            this.chkIsCRSEnabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIsCRSEnabled.Location = new System.Drawing.Point(633, 224);
            this.chkIsCRSEnabled.Name = "chkIsCRSEnabled";
            this.chkIsCRSEnabled.Size = new System.Drawing.Size(141, 20);
            this.chkIsCRSEnabled.TabIndex = 61;
            this.chkIsCRSEnabled.Text = "Is Enabled  FXCRS";
            this.chkIsCRSEnabled.UseVisualStyleBackColor = true;
            // 
            // FrmDistributionConfigMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 425);
            this.Controls.Add(this.chkIsCRSEnabled);
            this.Controls.Add(this.chkIsFOMEnaled);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnChannalSave);
            this.Controls.Add(this.cmbSavePriority);
            this.Controls.Add(this.chkIsEnabled);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtGroupCode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPropertyCode);
            this.Controls.Add(this.lblPropertyId);
            this.Controls.Add(this.txtPropertyId);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPmsCustCode);
            this.Controls.Add(this.btnInterfaceConfig);
            this.Controls.Add(this.btnParameterConfig);
            this.Name = "FrmDistributionConfigMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Distribution Config";
            this.Load += new System.EventHandler(this.FrmDistributionConfigMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnParameterConfig;
        private System.Windows.Forms.Button btnInterfaceConfig;
        private System.Windows.Forms.TextBox txtPmsCustCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPropertyId;
        private System.Windows.Forms.TextBox txtPropertyId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPropertyCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtGroupCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkIsEnabled;
        private System.Windows.Forms.ComboBox cmbSavePriority;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnChannalSave;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox chkIsFOMEnaled;
        private System.Windows.Forms.CheckBox chkIsCRSEnabled;
    }
}