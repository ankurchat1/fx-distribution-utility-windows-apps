﻿using DistributionConfiguration.BusinessObject.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DistributionConfiguration
{
    public partial class FrmDistributionConfigMain : Form
    {
        public static string _dbConnection = string.Empty;       
        public static string FullPathDB = string.Empty;
        public static string _SET_PMSCustCode = "";
        public static string _SET_PropertyID = "";

        public static string _SETGroupCode = "";
        

        public FrmDistributionConfigMain()
        {
            InitializeComponent();
        }

        public FrmDistributionConfigMain(string _connecttion)
        {
            _dbConnection = _connecttion;
            InitializeComponent();
        }

        public FrmDistributionConfigMain(string _connecttion, string _pmsCustCode, string _propertyId,string _grpCode)
        {
            _dbConnection = _connecttion;
            _SET_PMSCustCode = _pmsCustCode;
            _SET_PropertyID = _propertyId;
            _SETGroupCode = _grpCode;




            InitializeComponent();
        }


        private void btnParameterConfig_Click(object sender, EventArgs e)
        {
            FullPathDB = _dbConnection;

            _SET_PMSCustCode = txtPmsCustCode.Text.ToString();
            _SET_PropertyID = txtPropertyId.Text.ToString();
            _SETGroupCode = txtGroupCode.Text.ToString();


            ParameterConfigForm obj1 = new ParameterConfigForm(FullPathDB, _SET_PMSCustCode, _SET_PropertyID,_SETGroupCode);
            obj1.Show();
            this.Hide();
        }

        private void btnFXCRSConfig_Click(object sender, EventArgs e)
        {
            FullPathDB = _dbConnection;
            //FxCRSConfig obj1 = new FxCRSConfig(FullPathDB);
            //obj1.Show();
            this.Hide();
        }

        private void FxOnePropertyConfig_Click(object sender, EventArgs e)
        {
            FullPathDB = _dbConnection;
            //FxOnePropertyConfig obj1 = new FxOnePropertyConfig(FullPathDB);
            //obj1.Show();
            this.Hide();
        }

        private void btnInterfaceConfig_Click(object sender, EventArgs e)
        {
            FullPathDB = _dbConnection;

            _SET_PMSCustCode = txtPmsCustCode.Text.ToString();
            _SET_PropertyID = txtPropertyId.Text.ToString();
            _SETGroupCode = txtGroupCode.Text.ToString();

            ChannelInterfaceConfig obj1 = new ChannelInterfaceConfig(FullPathDB, _SET_PMSCustCode, _SET_PropertyID,_SETGroupCode);            
            obj1.Show();
            this.Hide();
        }

        private void FrmDistributionConfigMain_Load(object sender, EventArgs e)
        {
            LoadControl();
        }

        private void LoadControl()
        {
            cmbSavePriority.SelectedIndex = 0;           
            chkIsEnabled.Checked = false;
            txtPmsCustCode.Text = _SET_PMSCustCode.ToString();
            txtPropertyId.Text = _SET_PropertyID.ToString();
            txtGroupCode.Text = _SETGroupCode.ToString();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            //FullPathDB = _dbConnection;

            //_SET_PMSCustCode = txtPmsCustCode.Text.ToString();
            //_SET_PropertyID = txtPropertyId.Text.ToString();

            //FrmDistributionConfigMain obj1 = new FrmDistributionConfigMain(FullPathDB);
            //obj1.Show();
            this.Close();
        }

        private void btnChannalSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPmsCustCode.Text == string.Empty)
                {
                    MessageBox.Show("Please enter PMS Customer Code !!");
                    return;
                }
                else if (txtPropertyId.Text == string.Empty)
                {
                    MessageBox.Show("Please enter Property ID !!");
                    return;
                }
                else if (txtPropertyCode.Text == string.Empty)
                {
                    MessageBox.Show("Please enter Property Code !!");
                    return;
                }
                else if (txtGroupCode.Text == string.Empty)
                {
                    MessageBox.Show("Please enter Group Code !!");
                    return;
                }
                //else if (txtProductCode.Text == string.Empty)
                //{
                //    MessageBox.Show("Please enter Product Code  !!");
                //    return;
                //}
                else if (cmbSavePriority.SelectedIndex == 0)
                {
                    MessageBox.Show("Please Select the Save Priority !!");
                    return;
                }
                else
                {                    

                    FxCrsMappingSetting _crsMapper = new FxCrsMappingSetting();                    
                    _crsMapper.PmsCustCode = Int64.Parse(txtPmsCustCode.Text.ToString());                    
                    _crsMapper.FxCrsId = Int64.Parse(txtPropertyId.Text.ToString());
                    _crsMapper.PropertyCode = "10001";
                    if (chkIsEnabled.Checked == true)
                        _crsMapper.IsDHInventoryEnable = true;
                    else
                        _crsMapper.IsDHInventoryEnable = false;
                    _crsMapper.ModifyDatetime = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));
                    _crsMapper.UserId = "interface@idsnext.com";
                    _crsMapper.IsActive = true;

                    using (SqlConnection con = new SqlConnection(_dbConnection))
                    {
                        string query = "insert into FxCrsMapping values (@ID,@Pmscustcode,@FxCrsId,@PropertyCode,@IsDHInventoryEnable,@ModifiedDateTime,@UserId,@IsActive)";
                        using (SqlCommand cmd = new SqlCommand(query))
                        {
                            cmd.Connection = con;
                            cmd.Parameters.AddWithValue("@ID", _crsMapper.ID);
                            cmd.Parameters.AddWithValue("@Pmscustcode", _crsMapper.PmsCustCode);
                            cmd.Parameters.AddWithValue("@FxCrsId", _crsMapper.FxCrsId);
                            cmd.Parameters.AddWithValue("@PropertyCode", _crsMapper.PropertyCode);
                            cmd.Parameters.AddWithValue("@IsDHInventoryEnable", _crsMapper.IsDHInventoryEnable);
                            cmd.Parameters.AddWithValue("@ModifiedDateTime", _crsMapper.ModifyDatetime);
                            cmd.Parameters.AddWithValue("@UserId", _crsMapper.UserId);
                            cmd.Parameters.AddWithValue("@IsActive", _crsMapper.IsActive);

                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    FxOnePropertySetting _fx1PropertyMapper = new FxOnePropertySetting();

                    _fx1PropertyMapper.PmsCustCode = Int64.Parse(txtPmsCustCode.Text.ToString());
                    _fx1PropertyMapper.PropertyID = Int64.Parse(txtPropertyId.Text.ToString());
                    _fx1PropertyMapper.GroupCode = Int64.Parse(txtGroupCode.Text.ToString());
                    _fx1PropertyMapper.ProductCode = 10001; //Int64.Parse(txtProductCode.Text.ToString());
                    _fx1PropertyMapper.SavePriority = cmbSavePriority.SelectedItem.ToString();

                    if (chkIsCRSEnabled.Checked == true)
                        _fx1PropertyMapper.IsFXCRSEnabled = true;
                    else
                    {
                        _fx1PropertyMapper.IsFXCRSEnabled = false;
                    }

                    if (chkIsFOMEnaled.Checked == true)
                        _fx1PropertyMapper.IsFXFOMEnabled = true;
                    else {
                        _fx1PropertyMapper.IsFXFOMEnabled = false;
                    }
                    if (cmbSavePriority.SelectedItem.ToString() == "FOM")
                    {
                        _fx1PropertyMapper.IsFXFOMEnabled = true;
                        _fx1PropertyMapper.IsFXCRSEnabled = false;

                    }
                    if (cmbSavePriority.SelectedItem.ToString() == "FXCRS")
                    {
                        _fx1PropertyMapper.IsFXFOMEnabled = false;
                        _fx1PropertyMapper.IsFXCRSEnabled = true;
                    }

                    _fx1PropertyMapper.ModifyDatetime = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));
                    _fx1PropertyMapper.UserID = "interface@idsnext.com";


                    using (SqlConnection con = new SqlConnection(_dbConnection))
                    {
                        string query = "insert into FxDist_FxOnePropertyDetails values (@PropertyID,@PmsCustCode,@GroupCode,@ProductCode,@IsFXCRSEnabled,@IsFXFOMEnabled,@SavePriority,@UserId,@ModifiedDateTime)";
                        using (SqlCommand cmd = new SqlCommand(query))
                        {
                            cmd.Connection = con;
                            cmd.Parameters.AddWithValue("@PropertyID", _fx1PropertyMapper.PropertyID);
                            cmd.Parameters.AddWithValue("@PmsCustCode", _fx1PropertyMapper.PmsCustCode);
                            cmd.Parameters.AddWithValue("@GroupCode", _fx1PropertyMapper.GroupCode);
                            cmd.Parameters.AddWithValue("@ProductCode", _fx1PropertyMapper.ProductCode);
                            cmd.Parameters.AddWithValue("@IsFXCRSEnabled", _fx1PropertyMapper.IsFXCRSEnabled);
                            cmd.Parameters.AddWithValue("@IsFXFOMEnabled", _fx1PropertyMapper.IsFXFOMEnabled);
                            cmd.Parameters.AddWithValue("@SavePriority", _fx1PropertyMapper.SavePriority);
                            cmd.Parameters.AddWithValue("@UserId", _fx1PropertyMapper.UserID);
                            cmd.Parameters.AddWithValue("@ModifiedDateTime", _fx1PropertyMapper.ModifyDatetime);


                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                    MessageBox.Show("FX1 Property Configuration-CRS Mapping Saved !! ");
                    UnloadControl();




                }
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }
        }

        private void UnloadControl()
        {
            txtPmsCustCode.Text = "";
            txtPropertyId.Text = "";
            txtPropertyCode.Text = "";
            txtGroupCode.Text = "";
           // txtProductCode.Text = "";
        }

        private void txtPmsCustCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            
        }

        private void txtPropertyId_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtGroupCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtProductCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }        


        private MasterDataRequest GetMasterDatafromDB(string _PmsCustCode)
        {
            MasterDataRequest master = new MasterDataRequest();
            string _fxOneConnecttion = "Server=fxoneadmin.database.windows.net;Database=FXOneAdmin;User ID=fxoneadmin;Password=FXOneAdm@123;Trusted_Connection=False";


            string SqlString = "SELECT ID,PMSCode,GroupCode FROM tbl_Property where PMSCode="+ _PmsCustCode;
            using (SqlConnection conn = new SqlConnection(_fxOneConnecttion))
            {
                using (SqlCommand cmd = new SqlCommand(SqlString, conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("PMSCode", _PmsCustCode);

                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {                        
                        {
                            while (reader.Read())
                            {
                                master.PropertyId = reader["ID"].ToString();
                                master.PmsCustCode = reader["PMSCode"].ToString();
                                master.GroupCode = reader["GroupCode"].ToString();
                            }
                        }                       

                        reader.Close();
                    }                   
                }
                conn.Close();
            }

            return master;
        }

        private void txtPmsCustCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                MessageBox.Show("Please Click to Fetch Property Id, Group Code Information  !!");
                MasterDataRequest request =  GetMasterDatafromDB(txtPmsCustCode.Text.ToString());
                txtPropertyId.Text = request.PropertyId;
                txtGroupCode.Text = request.GroupCode;

            }

            
        }

        void RefreshCache()
        {
            for (int i = 0; i < 1000; i++)
            {
                Thread.Sleep(10);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DHResponseModel dhRefreshCache = new DHResponseModel();
            string Url = ""; ;
            dhRefreshCache = SharkPost(Url, "");

            using (FrmWait frm = new FrmWait(RefreshCache))
            {
                frm.ShowDialog(this);
            }



            //if (dhRefreshCache.MESSAGE == "Success")
            //{
            //    Thread.Sleep(10000);
            //    MessageBox.Show("Cache Refreshed.....");
            //}          



        }

        public static DHResponseModel SharkPost(string Url, object model)
        {
            var _Output = new DHResponseModel();
            Url = "https://qafxdistributioncachemanagement.azurewebsites.net/api/Cache/RefreshCacheData";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Url);
                    var Records = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                    var response = client.PostAsync(Url, Records).Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        _Output.STATUS = true;
                        _Output.MESSAGE = response.StatusCode.ToString();
                    }
                    else
                    {
                        var responseContent = response.Content;
                        _Output.STATUS = true;
                        if (response.StatusCode.ToString() == "OK")
                            _Output.MESSAGE = "Success";

                        //_Output = _OutputConvertToString;
                    }
                }
            }
            catch (Exception ex)
            {
                _Output.STATUS = false;
                _Output.MESSAGE = ex.Message.ToString();
            }

            return _Output;

        }

        public class DHResponseModel
        {
            public bool STATUS { get; set; }
            public string MESSAGE { get; set; }
        }

        
    }
}
