﻿using DBShark.Core.V1;
using DistributionConfiguration.BusinessObject;
using DistributionConfiguration.BusinessObject.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DistributionConfiguration
{
    public partial class ParameterConfigForm : Form
    {

        public static string _dbConnection = string.Empty;
        private static string Excel03ConString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0 Xml;HDR=YES'";
        public static string FullPathDB = string.Empty;

        public static string _GroupCode = string.Empty;

        public static string _SET_PMSCustCode = "";
        public static string _SET_PropertyID = "";
        public static string _SET_GroupCode = "";
        

        public ParameterConfigForm()
        {
            InitializeComponent();
        }

        public ParameterConfigForm(string _connecttion, string _pmsCustCode, string _propertyId,string _grpCode)
        {
            _dbConnection = _connecttion;
            _SET_PMSCustCode = _pmsCustCode;
            _SET_PropertyID = _propertyId;
            _SET_GroupCode = _grpCode;



            InitializeComponent();
        }

        public static string _filePath { get; set; }
        public static string _targetSQLDb { get; set; }

        private void BrowseButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog od = new OpenFileDialog();
            od.Filter = "Excell|*.xls;*.xlsx;";
            DialogResult dr = od.ShowDialog();
            if (dr == DialogResult.Abort)
                return;
            if (dr == DialogResult.Cancel)
                return;
            txtBrowse.Text = od.FileName.ToString();
            _filePath = txtBrowse.Text;
        }


        public static List<DataTable> RetrieveSourceData()
        {
            List<DataTable> sourceData = new List<DataTable>();

            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            DataTable dt4 = new DataTable();
            DataTable dt5 = new DataTable();
            DataTable dt6 = new DataTable();

            dt1 =  FetchData.RetrieveSourceDataAddon(_filePath);
            dt2 = FetchData.RetrieveSourceDataPickDrop(_filePath);
            dt3 = FetchData.RetrieveSourceDataBillingInstruction(_filePath);
            dt4 = FetchData.RetrieveSourceDataBusinessSource(_filePath);
            dt5 = FetchData.RetrieveSourceDataMarketSegment(_filePath);
            dt6 = FetchData.RetrieveSourceDataPaymentMode(_filePath);


            sourceData.Add(dt1);
            sourceData.Add(dt2);
            sourceData.Add(dt3);
            sourceData.Add(dt4);
            sourceData.Add(dt5);
            sourceData.Add(dt6);

            //string sourceConnString = string.Format(Excel03ConString, _filePath, "YES");
            //DataTable sourceData = new DataTable();
            //using (OleDbConnection conn = new OleDbConnection(sourceConnString))
            //{
            //    conn.Open();
            //    // Get the data from the source table as a SqlDataReader.
            //    OleDbCommand command = new OleDbCommand(
            //                        @"SELECT [ID],[Pmscustcode],[PropertyId],[IFSCCode],[Code],[Description],[Thirdpartycode],[ThirdpartycodeDesc] 
            //                         FROM [AddOn$]", conn);
            //    DataColumn dataColumn = new DataColumn();
            //    OleDbDataAdapter adapter = new OleDbDataAdapter(command);
            //    adapter.Fill(sourceData);
            //    conn.Close();
            //}          

            return sourceData;
        }

        public static void CopyData(List<DataTable> sourceData)
        {
            //GetValue<T>(sourceData);
            var _Output = string.Empty;
            string destConnString = _dbConnection;

            List<FXPMSAddOnMapping> Addonmapping = new List<FXPMSAddOnMapping>();
            Addonmapping = CommonMethod.ConvertToList<FXPMSAddOnMapping>(sourceData[0]);

            List<FXPMSPickupDropMapping> Pickdropmapping = new List<FXPMSPickupDropMapping>();
            Pickdropmapping = CommonMethod.ConvertToList<FXPMSPickupDropMapping>(sourceData[1]);

            List<FXPMSBillingInstructionMapping> BillignInsmapping = new List<FXPMSBillingInstructionMapping>();
            BillignInsmapping = CommonMethod.ConvertToList<FXPMSBillingInstructionMapping>(sourceData[2]);

            List<FXPMSBusinessSourceMapping> BusinessSourcemapping = new List<FXPMSBusinessSourceMapping>();
            BusinessSourcemapping = CommonMethod.ConvertToList<FXPMSBusinessSourceMapping>(sourceData[3]);

            List<FXPMSMarketSegmentMapping> MarketSegmentMapping = new List<FXPMSMarketSegmentMapping>();
            MarketSegmentMapping = CommonMethod.ConvertToList<FXPMSMarketSegmentMapping>(sourceData[4]);


            var addone = _ProcessAddOn(Addonmapping);
            var pickdrop = _ProcessPickupDrop(Pickdropmapping);
            var billingIns = _ProcessBillingInstruction(BillignInsmapping);
            var businesssource = _ProcessBusinessSource(BusinessSourcemapping);
            var marketsegment = _ProcessMarketSegment(MarketSegmentMapping);



            var _SerilizeAddon = JsonConvert.SerializeObject(addone);
            var _SerilizePickupDrop = JsonConvert.SerializeObject(pickdrop);
            var _SerilizeBilling = JsonConvert.SerializeObject(billingIns);
            var _SerilizeBusinessSource = JsonConvert.SerializeObject(businesssource);
            var _SerializeMarketSegment = JsonConvert.SerializeObject(marketsegment);


            try
            {
                using (SqlConnection con = new SqlConnection(destConnString))
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[SP_Distribution_Parameters_Configurations]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 200000;
                    cmd.Parameters.Add(new SqlParameter("@FxDistAddOn", _SerilizeAddon));
                    cmd.Parameters.Add(new SqlParameter("@FxDistPickupDrop", _SerilizePickupDrop));
                    cmd.Parameters.Add(new SqlParameter("@FxDistBilling", _SerilizeBilling));
                    cmd.Parameters.Add(new SqlParameter("@FxDistBusinessSource", _SerilizeBusinessSource));
                    cmd.Parameters.Add(new SqlParameter("@FxDistMarketSegment", _SerializeMarketSegment));

                    con.Open();
                    cmd.ExecuteReader();
                    con.Close();
                }
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }

            
        }

        private static List<FxDist_AddonMapping> _ProcessAddOn(List<FXPMSAddOnMapping> mapping)
        {
            List<FxDist_AddonMapping> _fxAddonList = new List<FxDist_AddonMapping>();
            

            foreach (var item in mapping)
            {
                FxDist_AddonMapping AddonData = new FxDist_AddonMapping();
                AddonData.Pmscustcode = Convert.ToInt64(_SET_PMSCustCode.ToString()); //Convert.ToInt64(item.Pmscustcode);
                AddonData.PropertyId = Convert.ToInt64(_SET_PropertyID.ToString());
                AddonData.IFSCCode = item.IFSCCode.ToString();
                AddonData.Code = item.Code.ToString();
                AddonData.Description = item.Description.ToString();
                AddonData.Thirdpartycode = item.Thirdpartycode.ToString();
                AddonData.ThirdpartycodeDesc = item.ThirdpartycodeDesc.ToString();
                AddonData.ModifyDateTime = DateTime.Now;
                AddonData.UserId = "interface@idsnext.com";
                AddonData.IsDefault = false;                AddonData.IsActive = true;

                _fxAddonList.Add(AddonData);
            }            

            return _fxAddonList;
        }

        private static List<FxDist_PickupDropMapping> _ProcessPickupDrop(List<FXPMSPickupDropMapping> mapping)
        {
            List<FxDist_PickupDropMapping> _fxPickupDropList = new List<FxDist_PickupDropMapping>();
            

            foreach (var item in mapping)
            {
                FxDist_PickupDropMapping PickDropData = new FxDist_PickupDropMapping();
                PickDropData.Pmscustcode = Convert.ToInt64(_SET_PMSCustCode.ToString()); //Convert.ToInt64(item.Pmscustcode);
                PickDropData.PropertyId = Convert.ToInt64(_SET_PropertyID.ToString());
                PickDropData.ParameterType = "DATAHUB";
                PickDropData.Code = "PCK";
                PickDropData.IFSCCode = item.IFSCCode.ToString();
                PickDropData.Pmscode = !string.IsNullOrEmpty(item.Code) ? item.Code : "";//item.Code.ToString();
                PickDropData.PmscodeDesc = !string.IsNullOrEmpty(item.Description) ? item.Description : "";//item.Description.ToString();
                PickDropData.Thirdpartycode = !string.IsNullOrEmpty(item.Thirdpartycode) ? item.Thirdpartycode : ""; //item.Thirdpartycode.ToString();
                PickDropData.ThirdpartycodeDesc = !string.IsNullOrEmpty(item.ThirdpartycodeDesc) ? item.ThirdpartycodeDesc : "";//item.ThirdpartycodeDesc.ToString();
                PickDropData.ModifyDateTime = DateTime.Now;
                PickDropData.UserId = "interface@idsnext.com";
                PickDropData.IsDefault = false;
                PickDropData.IsActive = true;

                _fxPickupDropList.Add(PickDropData);
            }

            return _fxPickupDropList;
        }

        private static List<FxDist_BillingInstructionMapping> _ProcessBillingInstruction(List<FXPMSBillingInstructionMapping> mapping)
        {
            List<FxDist_BillingInstructionMapping> _fxBillingList = new List<FxDist_BillingInstructionMapping>();
            

            foreach (var item in mapping)
            {
                FxDist_BillingInstructionMapping BillingInsData = new FxDist_BillingInstructionMapping();
                BillingInsData.Pmscustcode = Convert.ToInt64(_SET_PMSCustCode.ToString()); //Convert.ToInt64(item.Pmscustcode);
                BillingInsData.PropertyId = Convert.ToInt64(_SET_PropertyID.ToString());
                BillingInsData.IFSCCode = item.IFSCCode.ToString();
                BillingInsData.Pmscode = !string.IsNullOrEmpty(item.Code) ? item.Code : "";//item.Code.ToString();
                BillingInsData.Description = !string.IsNullOrEmpty(item.Description) ? item.Description : "";//item.Description.ToString();
                BillingInsData.Thirdpartycode = !string.IsNullOrEmpty(item.Thirdpartycode) ? item.Thirdpartycode : ""; //item.Thirdpartycode.ToString();
                BillingInsData.ThirdpartycodeDesc = !string.IsNullOrEmpty(item.ThirdpartycodeDesc) ? item.ThirdpartycodeDesc : "";//item.ThirdpartycodeDesc.ToString();
                BillingInsData.ModifyDateTime = DateTime.Now;
                BillingInsData.UserId = "interface@idsnext.com";
                BillingInsData.IsDefault = false;
                BillingInsData.IsActive = true;

                _fxBillingList.Add(BillingInsData);
            }

            return _fxBillingList;
        }

        private static List<FxDist_BusinessSourceMapping> _ProcessBusinessSource(List<FXPMSBusinessSourceMapping> mapping)
        {
            List<FxDist_BusinessSourceMapping> _fxBusinessSourceList = new List<FxDist_BusinessSourceMapping>();
            

            foreach (var item in mapping)
            {
                FxDist_BusinessSourceMapping BusinessSourceData = new FxDist_BusinessSourceMapping();
                BusinessSourceData.Pmscustcode = Convert.ToInt64(_SET_PMSCustCode.ToString()); //Convert.ToInt64(item.Pmscustcode);
                BusinessSourceData.PropertyId = Convert.ToInt64(_SET_PropertyID.ToString());
                BusinessSourceData.ParameterType = "DATAHUB";
                BusinessSourceData.IFSCCode = item.IFSCCode.ToString();
                BusinessSourceData.Pmscode = !string.IsNullOrEmpty(item.Code) ? item.Code : "";//item.Code.ToString();
                BusinessSourceData.PmscodeDesc =  !string.IsNullOrEmpty(item.Description) ? item.Description : "";//item.Description.ToString();
                BusinessSourceData.Thirdpartycode = !string.IsNullOrEmpty(item.Thirdpartycode) ? item.Thirdpartycode : ""; //item.Thirdpartycode.ToString();
                BusinessSourceData.ThirdpartycodeDesc = !string.IsNullOrEmpty(item.ThirdpartycodeDesc) ? item.ThirdpartycodeDesc : "";//item.ThirdpartycodeDesc.ToString();
                BusinessSourceData.ModifyDateTime = DateTime.Now;
                BusinessSourceData.UserId = "interface@idsnext.com";
                //BusinessSourceData = false;
                BusinessSourceData.IsActive = true;

                _fxBusinessSourceList.Add(BusinessSourceData);
            }

            return _fxBusinessSourceList;
        }


        private static List<FxDist_MarketSegmentMapping> _ProcessMarketSegment(List<FXPMSMarketSegmentMapping> mapping)
        {
            List<FxDist_MarketSegmentMapping> _fxMarketSegmentList = new List<FxDist_MarketSegmentMapping>();
            

            foreach (var item in mapping)
            {
                FxDist_MarketSegmentMapping MarketSegmentData = new FxDist_MarketSegmentMapping();
                MarketSegmentData.Pmscustcode = Convert.ToInt64(_SET_PMSCustCode.ToString()); //Convert.ToInt64(item.Pmscustcode);
                MarketSegmentData.PropertyId = Convert.ToInt64(_SET_PropertyID.ToString());
                MarketSegmentData.ParameterType = "DATAHUB";
                MarketSegmentData.IFSCCode = item.IFSCCode.ToString();
                MarketSegmentData.Pmscode = !string.IsNullOrEmpty(item.Code) ? item.Code : "";//item.Code.ToString();
                MarketSegmentData.PmscodeDesc = !string.IsNullOrEmpty(item.Description) ? item.Description : "";//item.Description.ToString();
                MarketSegmentData.Thirdpartycode = !string.IsNullOrEmpty(item.Thirdpartycode) ? item.Thirdpartycode : ""; //item.Thirdpartycode.ToString();
                MarketSegmentData.ThirdpartycodeDesc = !string.IsNullOrEmpty(item.ThirdpartycodeDesc) ? item.ThirdpartycodeDesc : "";//item.ThirdpartycodeDesc.ToString();
                MarketSegmentData.ModifyDateTime = DateTime.Now;
                MarketSegmentData.UserId = "interface@idsnext.com";
                //MarketSegmentData
                MarketSegmentData.IsActive = true;

                _fxMarketSegmentList.Add(MarketSegmentData);
            }

            return _fxMarketSegmentList;
        }




        private void btnParameterCancel_Click(object sender, EventArgs e)
        {
            FullPathDB = _dbConnection;

            _SET_PMSCustCode = txtPMSCustCode.Text.ToString();
            _SET_PropertyID = txtPropertyId.Text.ToString();
            

            FrmDistributionConfigMain obj1 = new FrmDistributionConfigMain(FullPathDB, _SET_PMSCustCode, _SET_PropertyID,_SET_GroupCode);
            obj1.Show();
            this.Close();
        }

        private void UploadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtBrowse.Text == string.Empty)
                {
                    MessageBox.Show("Please enter a value to Excel File !!");
                    return;
                }
                else
                {

                    //if (chkAddon.Checked)
                    //{
                        List<DataTable> data = RetrieveSourceData();
                        CopyData(data);
                    //}
                    //else if (chkPickupdop.Checked)
                    //{
                    //    DataTable data = RetrieveSourceDataAddon();
                    //    CopyDataAddOn(data);
                    //}
                    //else if (chkBillingIns.Checked)
                    //{
                    //    DataTable data = RetrieveSourceDataAddon();
                    //    CopyDataAddOn(data);
                    //}                    
                    

                    MessageBox.Show("File imported into sql server.");
                }
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }
            
            
            
           
        }

        private void ParameterConfigForm_Load(object sender, EventArgs e)
        {
            LoadControl();
        }

        public void LoadControl()
        {
            txtPMSCustCode.Text = FrmDistributionConfigMain._SET_PMSCustCode.ToString();
            txtPropertyId.Text = FrmDistributionConfigMain._SET_PropertyID.ToString();
            _GroupCode = _SET_GroupCode;
        }
    }
}
